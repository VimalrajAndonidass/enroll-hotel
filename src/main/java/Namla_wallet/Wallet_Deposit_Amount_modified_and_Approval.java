package Namla_wallet;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import multiScreenShot.MultiScreenShot;

public class Wallet_Deposit_Amount_modified_and_Approval {
	public static WebDriver driver = null;
	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"C:\\Automation\\Screenshots\\Enroll_Corporate\\FE\\English\\", "Wallet Deposit & Approval");
	static ExtentTest test;
	static ExtentReports report;

	@BeforeTest
	public void declaration() {
		// Webdriver declaration -new

		System.setProperty("webdriver.gecko.driver",
				System.getProperty("user.dir") + "/Drivers/FireFox/geckodriver.exe");
		driver = new FirefoxDriver();

		/*
		 * System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
		 * + "/Drivers/Chrome/chromedriver.exe"); driver = new ChromeDriver();
		 */
		// Report configuration
		report = new ExtentReports(
				"C:\\Automation\\Reports\\Enroll_Corporate\\FE\\English\\" + "Wallet Deposit_Approval.html");
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 0, invocationCount = 1)
	public void startTest() throws IOException {
		test = report.startTest("Language");
		try {
			test.log(LogStatus.PASS, "Namlatic Wallet Deposit - English");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 1)
	public void login() throws InterruptedException, AWTException, IOException {
		// driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		test = report.startTest("Login Verification");

		// Language change
		WebElement language = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]/div"));
		language.click();
		WebElement chglang = language.findElement(
				By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]"));
		chglang.click();
		try {

			// Login
			Thread.sleep(3000);
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));

//	WebElement nam_Login = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[5]/div"));
			nam_Login.click();
			WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]"));
			uname.sendKeys("ec@yopmail.com");
			WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]"));
			pwd.sendKeys("Test@123");
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}
	}

	@Test(priority = 2)
	void Namla_wallet() {
		test = report.startTest("Wallet Page Verification");

		try {
			Thread.sleep(3000);

			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement menu = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div[1]/div"));
			Actions builder = new Actions(driver);
			builder.moveToElement(menu).build().perform();
			new WebDriverWait(driver, 5);
			Thread.sleep(4000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div[2]/ul/li[3]")).click();
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Wallet Page loaded successfully");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Facing problem in loading Wallet Page");
		}
	}

	@Test(priority = 3)
	void Deposit() throws InterruptedException {
		test = report.startTest("Deposit voucher Verification");
		try {
			Thread.sleep(3000);
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));
			Thread.sleep(3000);
			driver.findElement(By.xpath("//*[@id='all']")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div/div[2]/div/div")).click();
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Vocuher deposit page successfully");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Facing problem in loading Vocuher deposit page");

		}
		try {
			Thread.sleep(3000);
			WebElement wallet_dep = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div/div[4]/input"));
			wallet_dep.sendKeys("51000.50");
			wallet_dep.sendKeys(Keys.TAB);
			driver.switchTo().parentFrame(); // Parent Frame navigation
			Thread.sleep(3000);
			/*
			 * driver.findElement(By.xpath("//button[@class='leadinModal-close']")).click();
			 * driver.switchTo().frame(driver.findElement(By.
			 * xpath("//iframe[@title='chat widget']"))); Thread.sleep(2000);
			 * driver.switchTo().parentFrame(); // Parent Frame
			 */
			((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div/div[5]/div")).click();
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Able to enter the deposit amount successfully");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to enter deposit amount");
		}

		/*
		 * try { Thread.sleep(3000);
		 * driver.switchTo().frame(driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div/iframe"))); Thread.sleep(2000);
		 * driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div[1]/wrapper")).click();
		 *
		 * driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div[2]/wrapper")).click();
		 * Thread.sleep(3000); multiScreens.multiScreenShot(driver);
		 * test.log(LogStatus.PASS, "Able to download and sent voucher in email"); }
		 * catch (Exception e) { test.log(LogStatus.FAIL,
		 * "Facing problem while downloading or sending voucher in email"); }
		 */

		Thread.sleep(3000);
		driver.switchTo().parentFrame(); // Parent Frame
		((JavascriptExecutor) driver).executeScript("scroll(0,570)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div[1]/div[4]/div[2]")).click();
		Thread.sleep(2000);
	}

	@Test(priority = 4)
	void receipt_upload() {
		test = report.startTest("Receipt upload request");

		try {

			// Removing hubspot
			driver.switchTo().parentFrame(); // Parent Frame
			Thread.sleep(2000);
			// driver.findElement(By.xpath("//button[@class='leadinModal-close']")).click();
			/*
			 * driver.switchTo().frame(driver.findElement(By.
			 * xpath("//iframe[@title='chat widget']")));
			 * driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).
			 * click(); Thread.sleep(2000);
			 * driver.findElement(By.xpath("/html/body/div[1]/div[1]/span/div/button")).
			 * click(); Thread.sleep(2000); driver.switchTo().parentFrame(); // Parent Frame
			 */

			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));

			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/div[1]/div/div[2]/div/div[8]/div/div/div"))
					.click();

			// Upload receipt
			Thread.sleep(3000);
			// driver.switchTo().parentFrame(); // Parent Frame

			// driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));
			Thread.sleep(3000);
			WebElement WTType = driver.findElement(By
					.xpath("/html/body/div[1]/div/div/div[2]/div/div/div/section/main/form/div[1]/div/div[2]/select"));
			WTType.click();
			WTType.sendKeys("Bank");
			WTType.click();

			Thread.sleep(3000);

			WebElement doc_upload = driver.findElement(By
					.xpath("/html/body/div[1]/div/div/div[2]/div/div/div/section/main/form/div[2]/div/label/span[2]"));
			System.out.println("outframe");
			doc_upload.click();

			Thread.sleep(3000);
			Robot rb = new Robot();
			// copying File path to Clipboard
			StringSelection str = new StringSelection("C:\\Automation\\Downloads\\NZOWO6X168X.pdf");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
			// press Contol+V for pasting
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			rb.delay(1000);
			rb.keyPress(KeyEvent.VK_CONTROL);
			rb.keyPress(KeyEvent.VK_V);
			rb.delay(1000);
			// release Control+V for pasting
			rb.keyRelease(KeyEvent.VK_CONTROL);
			rb.keyRelease(KeyEvent.VK_V);
			rb.delay(1000);
			// for pressing and releasing Enter
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			rb.delay(1000);
			multiScreens.multiScreenShot(driver);

			Thread.sleep(3000);
			WebElement WTrcno = driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[2]/div/div/div/section/main/form/div[3]/div/div/div[2]/input"));
			WTrcno.sendKeys("12345678900");

			WebElement scroll_sub = driver.findElement(
					By.xpath("/html/body/div[1]/div/div/div[2]/div/div/div/section/main/form/div[4]/div[2]/div"));
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll_sub);

			WebElement submit = driver.findElement(
					By.xpath("/html/body/div[1]/div/div/div[2]/div/div/div/section/main/form/div[4]/div[2]/div"));
			Thread.sleep(5000);
			multiScreens.multiScreenShot(driver);
			submit.click();
			test.log(LogStatus.PASS, "Successfully uploaded the receipt");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Successfully uploaded the receipt");

		}
	}

	@Test(priority = 5) // Wallet Approval
	void Approval() throws InterruptedException, IOException, AWTException {
		try {
			test = report.startTest("Wallet Approval request");
			// driver.get("chrome://settings/clearBrowserData");

			driver.navigate().to("https://test-backoffice.namlatic.com/request");

			// driver.navigate().to("https://test.namlatic.com");
			driver.manage().window().maximize();

			// To zoom out 3 times

			Thread.sleep(3000);

			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/app-input[1]/div/div/input"))
					.clear();

			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/app-input[1]/div/div/input"))
					.sendKeys("admin@namlatic.com");

			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/app-input[2]/div/div/input"))
					.clear();

			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/app-input[2]/div/div/input"))
					.sendKeys("admin@namlatic.com");

			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/button")).click();

			test.log(LogStatus.PASS, "Back Office Page Loading properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Back Office Page not Loading properly");

		}

		try {

			Thread.sleep(3000);
			driver.navigate().refresh();
			Thread.sleep(3000);
			driver.navigate().refresh();
			Thread.sleep(3000);

			driver.findElement(By.xpath(
					"/html/body/app-root/mat-sidenav-container/mat-sidenav/div/div[2]/app-nav-menu/div/div[7]/a"))
					.click();

			driver.findElement(By.xpath(
					"/html/body/app-root/mat-sidenav-container/mat-sidenav/div/div[2]/app-nav-menu/div/div[5]/a"))
					.click();
			Thread.sleep(3000);
			driver.findElement(By.xpath(
					"/html/body/app-root/mat-sidenav-container/mat-sidenav/div/div[2]/app-nav-menu/div/div[2]/a"))
					.click();
			driver.findElement(By.xpath(
					"/html/body/app-root/mat-sidenav-container/mat-sidenav/div/div[2]/app-nav-menu/div/div[2]/a/span/div[2]/div/a[3]"))
					.click();

			Thread.sleep(3000);
			for (int i = 0; i < 3; i++) {
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_SUBTRACT);
				robot.keyRelease(KeyEvent.VK_SUBTRACT);
				robot.keyRelease(KeyEvent.VK_CONTROL);

			}

			test.log(LogStatus.PASS, "Request Tab loading properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Request Tab not loading properly");
		}

		try {

			// Update status - Approve property
			Thread.sleep(3000);
			driver.switchTo().frame(driver.findElement(By.xpath("//*[@id='wallet-approval']")));

			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[2]/div/wrapper/div/wrapper[1]/div/div[2]/div[2]/div/div/input"))
					.click();
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[2]/div/wrapper/div/wrapper[1]/div/div[2]/div[2]/div/div/input"))
					.clear();
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[2]/div/wrapper/div/wrapper[1]/div/div[2]/div[2]/div/div/input"))
					.sendKeys("1000");
			driver.findElement(By.xpath("//*[@id='Layer_1']")).click();

			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[2]/div/wrapper/div/wrapper[1]/div/div[2]/div[6]/div/div[2]/select"))
					.click();

			Thread.sleep(3000);
			driver.findElement(By.xpath(
					" /html/body/div[1]/div/div/div[2]/div/wrapper/div/wrapper[1]/div/div[2]/div[6]/div/div[2]/select/option[2]"))
					.click(); // .click();
			Thread.sleep(3000);

			Thread.sleep(3000);

			driver.findElement(By.xpath("//*[text()='Submit']")).click();
			Thread.sleep(5000);

			test.log(LogStatus.PASS, "Corporate Approved successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Corporate Approved successfully");
		}

		try {
			// Status verification in FE
			driver.navigate().to("https://test.namlatic.com");
			driver.manage().window().maximize();
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement menu = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div[1]/div"));
			Actions builder = new Actions(driver);
			builder.moveToElement(menu).build().perform();
			new WebDriverWait(driver, 5);
			Thread.sleep(4000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div[2]/ul/li[3]")).click();
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));
			Thread.sleep(3000);
			driver.findElement(By.xpath("//*[@id='all']")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//*[@id='credit']")).click();
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Verified the receipt approved status successfully");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Unable to view the status of the Receipt");

		}
	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		report.flush();
		report.close();
		// driver.quit();
	}
}
