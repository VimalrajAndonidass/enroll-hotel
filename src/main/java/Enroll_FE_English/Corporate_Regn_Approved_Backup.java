package Enroll_FE_English;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Fillo;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import multiScreenShot.MultiScreenShot;

public class Corporate_Regn_Approved_Backup {
	public static WebDriver driver = null;
	public static MultiScreenShot multiScreens = new MultiScreenShot(
			"C:\\Automation\\Screenshots\\Enroll_Corporate\\FE\\English\\", "Corporate Regn.");
	static ExtentTest test;
	static ExtentReports report;
	// Fillo
	Fillo fillo = new Fillo();

	@BeforeTest
	public void declaration() throws FilloException {
		// Webdriver declaration -new
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		// Report configuration
		report = new ExtentReports("C:\\Automation\\Reports\\Enroll_Corporate\\FE\\English\\" + "Corporate Regn.html");
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));

	}

	@Test(priority = 0, invocationCount = 1)
	public void startTest() throws IOException {
		test = report.startTest("Language");
		try {
			test.log(LogStatus.PASS, "Namlatic Enroll Corporate - English");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 1)
	public void login() throws InterruptedException, AWTException, IOException {
		driver.get("chrome://settings/clearBrowserData");

		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		test = report.startTest("Login Verification");

		// Language change
		WebElement language = driver
				.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[1]/div"));
		language.click();
		WebElement chglang = language.findElement(
				By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[3]/div/div[2]/div/div[2]/div[1]/div[2]/div[2]"));
		chglang.click();
		try {

			// Login
			Thread.sleep(3000);
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login / Signup')]"));

//	WebElement nam_Login = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[5]/div"));
			nam_Login.click();
			WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]"));
			uname.sendKeys("ec@yopmail.com");
			WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]"));
			pwd.sendKeys("Test@123");
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			Thread.sleep(2000);
			test.log(LogStatus.PASS, "Login Pass");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Login Failed");
		}
	}

	@Test(priority = 2)
	void Enrol() throws InterruptedException, AWTException, IOException {
		test = report.startTest("Enroll Hotel Button status");
		try {
			Thread.sleep(3000);
			WebElement encorporate = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[1]/div"));
			encorporate.click();
			multiScreens.multiScreenShot(driver);
			WebElement getstarted = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[8]/div/div"));
			getstarted.click();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);
			test.log(LogStatus.PASS, "Enroll Corporate selected successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to select Enroll Corporate");
		}
	}

	@Test(priority = 3)
	void basic_details() throws IOException, InterruptedException, AWTException {
		test = report.startTest("Basic details Tab verification");
		try {
			// Basic Info
			((JavascriptExecutor) driver).executeScript("scroll(0,400)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));
			test.log(LogStatus.PASS, "Basic details Tab loading properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Basic details Tab not loading properly");
		}

		// Corporate Office name
		try {
			driver.findElement(By.xpath("//input[contains(@placeholder,'Enter your Corporate office Name')]"))
					.sendKeys("Fiona Palace");
			test.log(LogStatus.PASS, "Corporate office name field available and allowed to enter the value properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either corporate office name field not available or unable to enter the value properly");
		}

		// Regn. ID
		try {
			driver.findElement(By.xpath("//input[contains(@placeholder,'Enter your Corporate registration ID')]"))
					.sendKeys("Corp001");
			test.log(LogStatus.PASS, "Regn. ID field available and allowed to enter the value properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Regn. ID field not available or not allowed to enter the value properly");
		}

		// Address
		try {
			driver.findElement(By.xpath("//input[contains(@placeholder,'Address')]")).sendKeys("Puducherry");
			test.log(LogStatus.PASS, "Address field available and allowed to enter the value properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Address field not available or not allowed to enter the value properly");
		}

		// Country
		try {
			WebElement country = driver.findElement(By.xpath("//input[contains(@placeholder,'-- select country --')]"));
			country.click();
			multiScreens.multiScreenShot(driver);
			((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
			WebElement country1 = driver.findElement(By.xpath("//*[(text()='France')]")); // Country selection
			country1.click();
			test.log(LogStatus.PASS, "Select country field available and allowed to enter the value properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either select country field not available or not allowed to enter the value properly");
		}

		// City
		try {
			driver.findElement(By.xpath("//input[contains(@placeholder,'-- select city --')]")).click(); // City
																											// selection
			driver.findElement(By.xpath("//*[(text()='Paris')]")).click();
			test.log(LogStatus.PASS, "Select city field available and allowed to enter the value properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either select city field not available or not allowed to enter the value properly");
		}

		// PinCode
		try {
			driver.findElement(By.xpath("//*[contains(@placeholder, 'Enter pin code')]")).sendKeys("650011"); // Pincode
																												// selection
			test.log(LogStatus.PASS, "Select pincode field available and allowed to enter the value properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either pincode city field not available or not allowed to enter the value properly");
		}

		// Primary user mobile no.
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		try {
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// Navigation

			WebElement Pr_no = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter mobile number')]"));
			Pr_no.isDisplayed(); // Primary
									// mobile
			test.log(LogStatus.PASS, "Primary mobile number field available" + Pr_no.getText());
			Pr_no.clear();
			Pr_no.sendKeys("98745632");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Primary mobile number field not available or not allowed to enter value");
		}

		// Primary email ID
		try {
			WebElement Pr_email = driver.findElement(By.xpath("//input[contains(@placeholder,'example@email.com')]"));
			Pr_email.isDisplayed();
			String Pr_email1 = Pr_email.getText();
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Primary mail ID field available" + Pr_email1);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Primary mail ID field not available or not allowed to enter value");

		}

		// Secondary Contact person 1
		driver.switchTo().parentFrame(); // Parent Frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@class='leadinModal-close']")).click();
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='chat widget']")));
		Thread.sleep(2000);
		driver.switchTo().parentFrame(); // Parent Frame
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		multiScreens.multiScreenShot(driver);

		try {
			driver.findElement(By.xpath("//*[(text()='Select')]")).click();
			driver.findElement(By.xpath("//*[(text()='Mrs.')]")).click();
			driver.findElement(By.xpath("//input[contains(@placeholder,'Enter first name')]")).sendKeys("Josy");
			driver.findElement(By.xpath("//input[contains(@placeholder,'Enter last name')]")).sendKeys("Vimal");
			driver.findElement(By.xpath("//*[(text()='--select designation--')]")).click();
			driver.findElement(By.xpath("//*[(text()='Manager')]")).click(); //// Designation
			driver.findElement(By.xpath("//input[contains(@placeholder,'Enter phone number')]")).sendKeys("989433229"); // Sec
																														// phone
																														// no.
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[4]/div/wrapper/div[2]/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[2]/div[2]/input"))
					.sendKeys("904723280"); // Sec mobile number
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[4]/div/wrapper/div[2]/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[3]/div[2]/input"))
					.sendKeys("example007@email.com"); // Secondary email
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[4]/div/wrapper/div[2]/div[2]/div[2]/div/wrapper/div[1]/div[5]/div[3]/div/input"))
					.click(); // Assign role
			test.log(LogStatus.PASS, "System allowed to enter the secondary contact details 1 successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"System not allowing to enter the secondary contact details 1, either required field is not available or unable to enter values");
		}

		/*
		 * // Add another contact details
		 *
		 * multiScreens.multiScreenShot(driver); driver.switchTo().parentFrame(); //
		 * Parent frame navigation Thread.sleep(3000); ((JavascriptExecutor)
		 * driver).executeScript("scroll(0,600)");
		 * driver.switchTo().frame(driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame // navigation
		 * Thread.sleep(3000); try { // driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[2]"
		 * ));
		 * driver.findElement(By.xpath("//*[(text()='Add another contact person')]")).
		 * click(); Thread.sleep(3000); multiScreens.multiScreenShot(driver);
		 * test.log(LogStatus.PASS, "Pass, to click on add another contact person "); }
		 * catch (AssertionError e) { test.log(LogStatus.FAIL,
		 * "Fail, to click on add another contact person"); } // Contact person 2
		 * driver.switchTo().parentFrame(); // Parent Frame navigation
		 * Thread.sleep(3000); ((JavascriptExecutor)
		 * driver).executeScript("scroll(0,600)");
		 * driver.switchTo().frame(driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame // navigation
		 * Thread.sleep(3000); multiScreens.multiScreenShot(driver);
		 *
		 * try { driver.findElement(By.xpath("//*[(text()='Select')]")).click();
		 * driver.findElement(By.xpath("//*[(text()='Mr.')]")).click(); //
		 * driver.findElement(By.xpath("//input[contains(@placeholder,'Enter first //
		 * name')]")).sendKeys("John"); driver.findElement(By.xpath(
		 * " /html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper[2]/div[1]/div[4]/div[2]/div[2]/input"
		 * )) .sendKeys("John"); //
		 * driver.findElement(By.xpath("//input[contains(@placeholder,'Enter last //
		 * name')]")).sendKeys("Roy"); driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper[2]/div[1]/div[4]/div[3]/div[2]/input"
		 * )) .sendKeys("Roy");
		 * driver.findElement(By.xpath("//*[(text()='--select designation--')]")).click(
		 * ); ; driver.findElement(By.xpath("//*[(text()='Staff')]")).click(); ////
		 * Designation //
		 * driver.findElement(By.xpath("//input[contains(@placeholder,'Enter phone //
		 * number')]")).sendKeys("999999999"); // Sec phone no.
		 * driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper[2]/div[1]/div[5]/div[1]/div[2]/input"
		 * )) .sendKeys("999999999"); driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper[2]/div[1]/div[5]/div[2]/div[2]/input"
		 * )) .sendKeys("888888888"); // Sec mobile number driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper[2]/div[1]/div[5]/div[3]/div[2]/input"
		 * )) .sendKeys("test01@yopmail.com"); // Secondary email
		 * driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper[2]/div[1]/div[6]/div[3]/div[2]/input"
		 * )) .click(); // Assign role test.log(LogStatus.PASS,
		 * "System allowed to enter the secondary contact details 2 successfully"); }
		 * catch (AssertionError e) { test.log(LogStatus.FAIL,
		 * "System not allowing to enter the secondary contact details 2, either required field is not available or unable to enter values"
		 * ); } // Contact person 3 and delete it multiScreens.multiScreenShot(driver);
		 * driver.switchTo().parentFrame(); // Parent frame navigation
		 * Thread.sleep(3000); ((JavascriptExecutor)
		 * driver).executeScript("scroll(0,900)");
		 * driver.switchTo().frame(driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame // navigation
		 * Thread.sleep(3000); try { // driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[2]"
		 * ));
		 * driver.findElement(By.xpath("//*[(text()='Add another contact person')]")).
		 * click(); Thread.sleep(3000); multiScreens.multiScreenShot(driver);
		 * test.log(LogStatus.PASS,
		 * "Pass, to click on add another contact person 3 and delete it "); } catch
		 * (AssertionError e) { test.log(LogStatus.FAIL,
		 * "Fail, to click on add another contact person 3 and delete it"); }
		 *
		 * driver.switchTo().parentFrame(); // Parent Frame navigation
		 * Thread.sleep(3000); ((JavascriptExecutor)
		 * driver).executeScript("scroll(0,1200)");
		 * driver.switchTo().frame(driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame // navigation
		 * Thread.sleep(3000); multiScreens.multiScreenShot(driver);
		 *
		 * try { driver.findElement( By.xpath(
		 * "/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper[3]/div[1]/div[1]"
		 * )) .click(); Thread.sleep(3000); multiScreens.multiScreenShot(driver);
		 * test.log(LogStatus.PASS,
		 * "System allowed to close/delete the contact details"); } catch
		 * (AssertionError e) { test.log(LogStatus.FAIL,
		 * "System not allowed to close/delete the contact details"); }
		 */

		// Document upload
		try {
			Thread.sleep(3000);
			driver.switchTo().parentFrame(); // Parent frame navigation
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,1100)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// navigation
			WebElement Imgupl3 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
			Thread.sleep(2000);

			Imgupl3.click();
			Thread.sleep(3000);
			Robot image3 = new Robot();
			// copying File path to Clipboard
			StringSelection str3 = new StringSelection("C:\\Automation\\Images\\img2.jpg");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str3, null);

			// press Contol+V for pasting
			image3.keyPress(KeyEvent.VK_CONTROL);
			image3.keyPress(KeyEvent.VK_V);

			// release Contol+V for pasting
			image3.keyRelease(KeyEvent.VK_CONTROL);
			image3.keyRelease(KeyEvent.VK_V);
			// for pressing and releasing Enter
			image3.keyPress(KeyEvent.VK_ENTER);
			image3.keyRelease(KeyEvent.VK_ENTER);
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "System allowing to add document");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "System not allowing to add document");
		}

		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		try {
			driver.findElement(By
					.xpath("/html/body/div[1]/div/div/div[4]/div/wrapper/div[1]/div[1]/div/div/div[2]/div[3]/div/div"))
					.click(); // Translate button selection and French convertion
			Thread.sleep(3000);
			test.log(LogStatus.PASS, "System allowing to translate to French");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "System not allowing to translate to French");
		}

		try {
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[4]/div/wrapper/div[1]/div[1]/div/div/div[2]/div[2]/div[3]/select"))
					.click(); // Selection of language DD
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[4]/div/wrapper/div[1]/div[1]/div/div/div[2]/div[2]/div[3]/select"))
					.sendKeys("AR"); // Arabic Convertion
			driver.findElement(By
					.xpath("/html/body/div[1]/div/div/div[4]/div/wrapper/div[1]/div[1]/div/div/div[2]/div[3]/div/div"))
					.click(); // Selecting Translate button
			test.log(LogStatus.PASS, "System allowing to translate to Arabic");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "System not allowing to translate to Arabic");
		}
		multiScreens.multiScreenShot(driver);
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1400)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// Navigation
		Thread.sleep(3000);

		try {
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/wrapper/div[3]/div/div[2]/div")).click(); // Next
			// Button
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "System allowing to proceed with 'Next' button");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "System not allowing to proceed with 'Next' button");
		}
	}

	@Test(priority = 4) // Read policies, terms & conditions tab verification
	void Terms_Conditions() throws InterruptedException, IOException, AWTException {
		test = report.startTest("Read policies, terms & conditions tab verification");
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// //
																												// navigation
		try {
			// Corporate Policy
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div[1]")).click();
			Thread.sleep(3000);
			driver.switchTo().parentFrame(); // Parent Frame Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// //
																													// navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div[1]")).click();

			test.log(LogStatus.PASS, "Corporate Policy loaded properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Corporate Policy is not loading properly");
		}

		try {
			// Cancellation Policy
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]")).click();
			Thread.sleep(3000);
			driver.switchTo().parentFrame(); // Parent Frame Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// //
																													// navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]")).click();

			test.log(LogStatus.PASS, "Cancellation Policy loaded properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Cancellation Policy is not loading properly");
		}

		try {
			// Terms and conditions Policy
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[3]/div[1]")).click();
			Thread.sleep(3000);
			driver.switchTo().parentFrame(); // Parent Frame Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// //
																													// navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[3]/div[1]")).click();

			test.log(LogStatus.PASS, "Terms & Conditions Policy loaded properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Terms & Conditions Policy is not loading properly");
		}

		test.log(LogStatus.PASS, "Read policies, terms & conditions tab is loading properly");

		// Accept policies checkbox selection
		try {
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[4]/div/input")).click(); // Terms
																												// and
																												// conditions
																												// checkbox
			test.log(LogStatus.PASS,
					"System allowing to select 'I read the policies and accept the terms & conditions' checkbox");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"System not allowing to select 'I read the policies and accept the terms & conditions' checkbox");
		}

		// Select 'Finish & Register' button
		try {
			Thread.sleep(3000);
			WebElement F_R = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[5]/div"));
			F_R.isEnabled();
			F_R.click();
			Thread.sleep(3000);
			test.log(LogStatus.PASS, "'Finish & Register' button enabled and allowing to select the button");
			// driver.close();
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "System not allowing to select 'Finish & Register' button or not enabled");
		}
	}

	@Test(priority = 5) // View enrollment
	void view_enroll() throws InterruptedException, IOException, AWTException {
		test = report.startTest("View enrollment verification");
		// driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");

		Thread.sleep(3000);

		try {
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement menu = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div[1]/div"));
			Actions builder = new Actions(driver);
			builder.moveToElement(menu).build().perform();
			new WebDriverWait(driver, 5);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div[2]/ul/li[3]")).click();

			WebElement Title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[1]"));
			System.out.println(Title.getText());

			Thread.sleep(3000);
			String Corp_name = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[3]/div[3]")).getText();
			String status = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[3]/div[5]/span"))
					.getText();
			WebElement view = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div[3]/div[9]/div[1]"));
			view.click();
			test.log(LogStatus.PASS,
					"View option working fine" + "<br/> Corporate name: " + Corp_name + "<br/> Status: " + status);
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "View option is not working fine");
		}

		try {
			// Basic Info
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));
			multiScreens.multiScreenShot(driver);

			driver.switchTo().parentFrame();
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,600)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));
			multiScreens.multiScreenShot(driver);

			driver.switchTo().parentFrame();
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,1500)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe")));
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/wrapper/div[3]/div/div[2]/div")).click(); // Next
			// Button
			multiScreens.multiScreenShot(driver);

			test.log(LogStatus.PASS, "Basic details Tab loading properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Basic details Tab not loading properly");
		}

		try {
			driver.switchTo().parentFrame(); // Parent Frame
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// //
																													// navigation

			// Corporate Policy
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div[1]")).click();
			Thread.sleep(3000);
			driver.switchTo().parentFrame(); // Parent Frame Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);

			((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);

			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// //
																													// navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div[1]")).click();

			// Cancellation Policy
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]")).click();
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);

			driver.switchTo().parentFrame(); // Parent Frame Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);

			((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);

			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// //
																													// navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]")).click();

			// Terms and conditions Policy
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[3]/div[1]")).click();
			Thread.sleep(3000);
			driver.switchTo().parentFrame(); // Parent Frame Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,500)");
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);

			((JavascriptExecutor) driver).executeScript("scroll(0,1000)");
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);

			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// //
																													// navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[3]/div[1]")).click();
			multiScreens.multiScreenShot(driver);

			driver.navigate().to("https://test.namlatic.com/");

			Thread.sleep(3000);

			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			WebElement menu = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div[1]/div"));
			Actions builder = new Actions(driver);
			builder.moveToElement(menu).build().perform();
			new WebDriverWait(driver, 5);
			driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[2]/div[2]/ul/li[3]")).click();
			driver.close();
			Thread.sleep(3000);
			test.log(LogStatus.PASS, "Read Policies, terms & conditions Tab loading properly");

		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Read Policies, terms & conditions Tab not loading properly");
		}
	}

	@Test(priority = 6) // View enrollment
	void Approval() throws InterruptedException, IOException, AWTException {
		try {
			test = report.startTest("Corporate Approval request");
			driver.get("chrome://settings/clearBrowserData");
			// driver.navigate().to("https://test.namlatic.com");
			// driver.manage().window().maximize();
			// To zoom out 3 times

			for (int i = 0; i < 3; i++) {
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_SUBTRACT);
				robot.keyRelease(KeyEvent.VK_SUBTRACT);
				robot.keyRelease(KeyEvent.VK_CONTROL);
			}

			Thread.sleep(3000);
			driver.navigate().to("https://test-backoffice.namlatic.com/request");

			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/app-input[1]/div/div/input"))
					.clear();

			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/app-input[1]/div/div/input"))
					.sendKeys("admin@namlatic.com");

			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/app-input[2]/div/div/input"))
					.clear();

			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/app-input[2]/div/div/input"))
					.sendKeys("admin@namlatic.com");

			driver.findElement(By.xpath("/html/body/app-root/app-login/div/div/div/div[2]/button")).click();

			test.log(LogStatus.PASS, "Back Office Page Loading properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Back Office Page not Loading properly");

		}
	}

	@Test(priority = 7)
	void Request_Menu() throws InterruptedException, AWTException {
		test = report.startTest("Request Tab validation");
		try {

			Thread.sleep(3000);
			driver.navigate().refresh();
			Thread.sleep(3000);
			driver.navigate().refresh();
			Thread.sleep(3000);
			driver.findElement(By.xpath(
					"/html/body/app-root/mat-sidenav-container/mat-sidenav/div/div[2]/app-nav-menu/div/div[5]/a"))
					.click();
			Thread.sleep(3000);
			driver.findElement(By.xpath(
					"/html/body/app-root/mat-sidenav-container/mat-sidenav/div/div[2]/app-nav-menu/div/div[2]/a"))
					.click();
			driver.findElement(By.xpath(
					"/html/body/app-root/mat-sidenav-container/mat-sidenav/div/div[2]/app-nav-menu/div/div[2]/a/span/div[2]/div/a[2]/span/label"))
					.click();

			test.log(LogStatus.PASS, "Request Tab loading properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Request Tab not loading properly");
		}
	}

	@Test(priority = 8)
	void Corporate_Approval() throws InterruptedException, AWTException {
		test = report.startTest("Corporate Approval");
		try {

			// Update status - Approve property

			Thread.sleep(3000);
			driver.findElement(By.xpath("(//div[contains(@class,'mat-select-arrow')])")).click(); // .click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//*[text()='Approve']")).click(); // .click();
			Thread.sleep(3000);

			Thread.sleep(3000);

			driver.findElement(By.xpath("//*[text()='Update']")).click();

			test.log(LogStatus.PASS, "Corporate Approved successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Corporate Approved successfully");
		}
	}

	void Namla_Deposit() {

	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		report.flush();
		report.close();
		// driver.quit();
	}
}
