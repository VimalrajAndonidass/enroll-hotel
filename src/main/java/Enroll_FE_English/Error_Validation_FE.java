package Enroll_FE_English;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import multiScreenShot.MultiScreenShot;

public class Error_Validation_FE {
	public static WebDriver driver = null;
	public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Automation\\Screenshots\\Enroll_Hotel\\FE\\",
			"Enroll_Hotel_Error_Validation");
	static ExtentTest test;
	static ExtentReports report;

	@BeforeTest
	public void declaration() {
		// Webdriver declaration
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		// Report configuration
		report = new ExtentReports(
				"C:\\Automation\\Reports\\Enroll_Hotel\\FE\\English\\" + "Enroll_Corporate_Error_Validation.html");
		report.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	@Test(priority = 0, invocationCount = 1)
	public void startTest() throws IOException {
		test = report.startTest("Enroll Hotel - Error Validation");
		try {
			test.log(LogStatus.PASS, "Enroll Hotel - Error Validation - English");
		} catch (AssertionError e) {

		}
	}

	@Test(priority = 1)
	public void login() throws InterruptedException, AWTException, IOException {
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		test = report.startTest("Login Verification");

//Language change

		/*
		 * WebElement language = driver.findElement(By.xpath(
		 * "/html/body/div[1]/div/div[1]/div/div[1]/div[4]/div/div/div/div")); if
		 * (language.getText().equals("Arabic") &&
		 * language.getText().equals("Fran�ais")) { language.click(); WebElement chglang
		 * = language.findElement(By.xpath("//*[(text()='English')]")); chglang.click();
		 * } else System.out.println("English");
		 */
		WebElement language = driver.findElement(By.xpath("//*[(text()='Fran�ais')]"));
		language.click();
		WebElement chglang = language.findElement(By.xpath("//*[(text()='English')]"));
		chglang.click();
		try {
			// Login
			Thread.sleep(3000);
			WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login')]"));

			// WebElement nam_Login =
			// driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[5]/div"));
			nam_Login.click();
			WebElement uname = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[4]/div/div[3]/div[1]/div[2]/input"));
			Thread.sleep(3000);
			uname.sendKeys("testt55@yopmail.com");
			WebElement pwd = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[4]/div/div[3]/div[2]/div[2]/input"));
			pwd.sendKeys("Test123456");
			multiScreens.multiScreenShot(driver);
			pwd.sendKeys(Keys.ENTER);
			test.log(LogStatus.PASS, "Logged in successfully");
			Thread.sleep(3000);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to Login successfully");
		}
	}

	@Test(priority = 2)
	void Enrol() throws InterruptedException, AWTException, IOException {
		test = report.startTest("Enroll Hotel Button status");
		try {
			Thread.sleep(3000);
			WebElement enhotel = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[1]/div"));
			System.out.println(enhotel.getText());
			enhotel.click();
			multiScreens.multiScreenShot(driver);
			WebElement getstarted = driver
					.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[6]/div/div"));
			getstarted.click();
			multiScreens.multiScreenShot(driver);
			Thread.sleep(3000);
			test.log(LogStatus.PASS, "Enroll Hotel selected successfully");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Unable to select Enroll hotel");
		}
	}

	@Test(priority = 3)
	void basic_details() throws IOException, InterruptedException {
		test = report.startTest("Basic details Tab verification");
		// Basic Info
		try {
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// Navigation
			test.log(LogStatus.PASS, "Basic details Tab loading properly");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Basic details Tab not loading properly");
			multiScreens.multiScreenShot(driver);
		}

		// Proceeding 'Save in draft' without entering 'Property name'
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// Navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[5]/div/div")).click();

		String Exp_Property_name = (driver.findElement(
				By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div/div[2]/div[2]/div[3]"))).getText();
		String Act_Property_name = "Title is required.";
		try {
			Assert.assertEquals(Exp_Property_name, Act_Property_name);
			test.log(LogStatus.PASS, "Property name is mandate and Error message matching with expected: " + "\""
					+ Act_Property_name + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Property name error message is not matching or the field is not marked as mandate."
							+ "Actual Result: " + Act_Property_name + "Expected Result: " + Exp_Property_name);
			multiScreens.multiScreenShot(driver);

		}

		// Proceeding 'Next' without entering any values
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[9]/div/div/div")).click(); // Next
																												// Button
		Thread.sleep(3000);

		// Enter Property name
		try {
			Assert.assertEquals(Exp_Property_name, Act_Property_name);
			test.log(LogStatus.PASS, "Property name is mandate and Error message matching with expected: " + "\""
					+ Act_Property_name + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Property name error message is not matching or the field is not marked as mandate."
							+ "Actual Result: " + Act_Property_name + "Expected Result: " + Exp_Property_name);
			multiScreens.multiScreenShot(driver);

		}

		// Rating field validation
		String Exp_Rating = (driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[2]/div/div[3]"))).getText();
		String Act_Rating = "Rating is required.";
		try {
			Assert.assertEquals(Exp_Rating, Act_Rating);
			test.log(LogStatus.PASS,
					"Rating field is mandate and Error message matching with expected: " + "\"" + Act_Rating + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Rating field is not available or not allowed to enter the value properly"
					+ "Actual Result: " + Act_Rating + " Expected Result: " + Exp_Rating);
			multiScreens.multiScreenShot(driver);

		}

		// Address field validation
		String Exp_Address = (driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[3]/div/div/div[2]/div[2]/div[3]")))
						.getText();
		String Act_Address = "Address is required.";
		try {
			Assert.assertEquals(Exp_Address, Act_Address);
			test.log(LogStatus.PASS,
					"Address field is mandate and Error message matching with expected: " + "\"" + Act_Address + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Address field is not available or not allowed to enter the value properly"
					+ "Actual Result: " + Act_Address + " Expected Result: " + Exp_Address);
			multiScreens.multiScreenShot(driver);

		}

		// Country field validation
		String Exp_country = (driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[4]/div[1]/div/div[3]"))).getText();
		String Act_country = "Country is required.";
		try {
			Assert.assertEquals(Exp_country, Act_country);
			test.log(LogStatus.PASS,
					"Country field is mandate and Error message matching with expected: " + "\"" + Act_country + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Country field is not available or not allowed to enter the value properly"
					+ "Actual Result: " + Act_Address + " Expected Result: " + Exp_Address);
			multiScreens.multiScreenShot(driver);

		}

		// City field validation
		String Exp_city = (driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[4]/div[2]/div/div[3]"))).getText();
		String Act_city = "City is required.";
		try {
			Assert.assertEquals(Exp_city, Act_city);
			test.log(LogStatus.PASS,
					"City field is mandate and Error message matching with expected: " + "\"" + Act_city + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either City field is not available or not allowed to enter the value properly"
					+ "Actual Result: " + Act_city + " Expected Result: " + Exp_city);
			multiScreens.multiScreenShot(driver);

		}

		// Pincode field validation
		String Exp_pincode = (driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[4]/div[3]/div/div[3]"))).getText();
		String Act_pincode = "Pincode is required.";
		try {
			Assert.assertEquals(Exp_city, Act_city);
			test.log(LogStatus.PASS,
					"Pincode field is mandate and Error message matching with expected: " + "\"" + Act_pincode + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Pincode field is not available or not allowed to enter the value properly"
					+ "Actual Result: " + Act_pincode + " Expected Result: " + Exp_pincode);
			multiScreens.multiScreenShot(driver);

		}

		// No. of rooms field validation
		String Exp_rooms = (driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[5]/div[2]/div/div[3]"))).getText();
		String Act_rooms = "Number Of Room is required.";
		try {
			Assert.assertEquals(Exp_city, Act_city);
			test.log(LogStatus.PASS, "No. of rooms field is mandate and Error message matching with expected: " + "\""
					+ Act_rooms + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either No. of rooms field is not available or not allowed to enter the value properly"
							+ "Actual Result: " + Act_rooms + " Expected Result: " + Exp_rooms);
			multiScreens.multiScreenShot(driver);

		}

		// Property Policy field validation
		String Exp_policies = (driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[6]/div/div/div[2]/div[2]/div[2]")))
						.getText();
		String Act_policies = "Policies is required.";
		try {
			Assert.assertEquals(Exp_policies, Act_policies);
			test.log(LogStatus.PASS, "Property Policies field is mandate and Error message matching with expected: "
					+ "\"" + Act_policies + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Property Policies field is not available or not allowed to enter the value properly"
							+ "Actual Result: " + Act_policies + " Expected Result: " + Exp_policies);
			multiScreens.multiScreenShot(driver);

		}

		// Property description field validation
		String Exp_description = (driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[7]/div/div/div[2]/div[2]/div[2]")))
						.getText();
		String Act_description = "Description is required.";
		try {
			Assert.assertEquals(Exp_description, Act_description);
			test.log(LogStatus.PASS, "Property Description field is mandate and Error message matching with expected: "
					+ "\"" + Act_description + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Property Description field is not available or not allowed to enter the value properly"
							+ "Actual Result: " + Act_description + " Expected Result: " + Exp_description);
			multiScreens.multiScreenShot(driver);

		}

		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		// Value updating for English language
		driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Property name')]")).sendKeys("Fiona Palace");
		driver.findElement(By.xpath("//input[contains(@placeholder,'N/A')]")).click();
		driver.findElement(By.xpath("//*[(text()='4 Star')]")).click();
		driver.findElement(By.xpath("//input[contains(@placeholder,'Address')]")).sendKeys("Puducherry");
		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		WebElement country = driver.findElement(By.xpath("//input[contains(@placeholder,'-- select country --')]"));
		country.click();
		WebElement country1 = driver.findElement(By.xpath("//*[(text()='France')]")); // Country selection
		country1.click();
		driver.findElement(By.xpath("//*[contains(@placeholder, 'Enter pin code')]")).sendKeys("650011"); // Pincode
																											// selection
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,600)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[contains(@placeholder,'-- select the type of property --')]")).click(); // Property
																														// selection
		driver.findElement(By.xpath("//*[(text()='Hotel')]")).click();
		WebElement roomcount = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter value')]")); // Total
																												// no.
																												// of
																												// rooms
																												// selection
		roomcount.sendKeys("10");
		((JavascriptExecutor) driver).executeScript("scroll(0,600)");
		driver.findElement(By.xpath("//input[contains(@placeholder,'-- select city --')]")).click(); // City selection
		WebElement city = driver.findElement(By.xpath("//*[(text()='Paris')]"));
		city.click();
		driver.findElement(By.xpath(
				" /html/body/div[1]/div/div/div[4]/div/div/div[6]/div/div/div[2]/div[2]/div[1]/div/div[2]/div[1]/p"))
				.sendKeys("Test"); // Text area - Property Policy
		driver.findElement(By.xpath(
				" /html/body/div[1]/div/div/div[4]/div/div/div[7]/div/div/div[2]/div[2]/div[1]/div/div[2]/div[1]/p"))
				.sendKeys("Test"); // Text area - Property description
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		// Save in draft validation without translating to French and Arabic
		driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[5]/div/div/div")).click(); // Save in Draft
		Thread.sleep(3000);
		String Exp_tr_message = "Please use AutoTranslate or manually enter missing fields in the language - [fr,ar]";
		String Act_tr_message = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div/div[2]/div[2]/div[3]"))
				.getText();
		try {
			Assert.assertEquals(Exp_tr_message, Act_tr_message);
			test.log(LogStatus.PASS,
					"System should not allow to proceed 'Save in draft' without translating the Property name to French and Arabic "
							+ "\"" + Act_tr_message + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either system allowing to proceed without translation or 'Error message displayed is incorrect"
							+ " Expected Result: " + "\"" + Exp_tr_message + "\"" + Act_tr_message + "\"");
			multiScreens.multiScreenShot(driver);

		}

		// Proceeding 'Next' button without translating to French and Arabic
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[9]/div/div/div")).click(); // Next
																												// Button
		Thread.sleep(3000);
		try {
			Assert.assertEquals(Exp_tr_message, Act_tr_message);
			test.log(LogStatus.PASS,
					"System should not allow to proceed 'Next' without translating the Property name to French and Arabic "
							+ "\"" + Act_tr_message + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either system allowing to proceed without translation or 'Error message displayed is incorrect"
							+ " Expected Result: " + "\"" + Exp_tr_message + "\"" + Act_tr_message + "\"");
			multiScreens.multiScreenShot(driver);

		}

		// Translating to French
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[3]/div"))
				.click(); // Translate button selection and French convertion

		// Save in draft validation without translating to Arabic
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[5]/div/div/div")).click(); // Save in Draft
		Thread.sleep(3000);
		try {
			Assert.assertEquals(Exp_tr_message, Act_tr_message);
			test.log(LogStatus.PASS,
					"System should not allow to proceed 'Save in draft' without translating the Property name to French and Arabic "
							+ "\"" + Act_tr_message + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either system allowing to proceed without translation or 'Error message displayed is incorrect"
							+ " Expected Result: " + "\"" + Exp_tr_message + "\"" + Act_tr_message + "\"");
			multiScreens.multiScreenShot(driver);

		}

		// Proceeding 'Next' button without translating to Arabic
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[9]/div/div/div")).click(); // Next
																												// Button
		Thread.sleep(3000);
		try {
			Assert.assertEquals(Exp_tr_message, Act_tr_message);
			test.log(LogStatus.PASS,
					"System should not allow to proceed 'Next' without translating the Property name to French and Arabic "
							+ "\"" + Act_tr_message + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either system allowing to proceed without translation or 'Error message displayed is incorrect"
							+ " Expected Result: " + "\"" + Exp_tr_message + "\"" + Act_tr_message + "\"");
			multiScreens.multiScreenShot(driver);

		}

		// Translating to Arabic
		driver.findElement(
				By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[2]/div[3]/select"))
				.click(); // Selection of language DD
		driver.findElement(
				By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[2]/div[3]/select"))
				.sendKeys("AR"); // Arabic Convertion
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[3]/div"))
				.click(); // Selecting Translate button

		// Save in draft validation after translating to French & Arabic
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[5]/div/div/div")).click(); // Save in Draft

		// Proceeding 'Next' button without translating to French & Arabic
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[9]/div/div/div")).click(); // Next
																												// Button

	}

	// Contact details tab verification
	@Test(priority = 4)
	void contact_details() throws InterruptedException, IOException {
		test = report.startTest("Contact details Tab verification");
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		try {
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// Navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[2]")).click(); // Navigation to Photo
																								// Gallery Tab
			test.log(LogStatus.PASS, "Contact details tab loading properly");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Contact details tab is not loading properly");
			multiScreens.multiScreenShot(driver);

		}

		// Proceeding 'Next' without entering any values
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div/div/div[3]/div")).click(); // Next
																														// Button
		Thread.sleep(3000);

		// Primary Mobile number validation
		String Act_Pr_Mobile = driver
				.findElement(By.xpath(
						"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[1]/div/div/div[2]/div[1]/div[3]"))
				.getText();
		String Exp_Pr_Mobile = "Mobile is required.";

		try {
			Assert.assertEquals(Exp_Pr_Mobile, Act_Pr_Mobile);
			test.log(LogStatus.PASS, "Primary mobile number field is mandate and Error message matching with expected "
					+ "\"" + Act_Pr_Mobile + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Primary mobile number error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_Pr_Mobile +\"Expected Result: \" + Exp_Pr_Mobile");
			multiScreens.multiScreenShot(driver);

		}

		// Primary email validation
		String Act_Pr_Mail = driver
				.findElement(By.xpath(
						"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[1]/div/div/div[2]/div[2]/div[3]"))
				.getText();
		String Exp_Pr_Mail = "Email is required.";

		try {
			Assert.assertEquals(Exp_Pr_Mail, Act_Pr_Mail);
			test.log(LogStatus.PASS, "Primary mail ID field is mandate and Error message matching with expected" + "\""
					+ Act_Pr_Mail + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Primary mail ID error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_Pr_Mobile +\"Expected Result: \" + Exp_Pr_Mobile");
			multiScreens.multiScreenShot(driver);

		}

		// Contact person 1 - Title verification
		String Act_sc_Title = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[3]/div[1]/div/div[3]"))
				.getText();
		String Exp_sc_Title = "Title is required.";

		try {
			Assert.assertEquals(Exp_sc_Title, Act_sc_Title);
			test.log(LogStatus.PASS, "Secondary Title field is mandate and Error message matching with expected" + "\""
					+ Act_sc_Title + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Secondary Title error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_Pr_Mobile +\"Expected Result: \" + Exp_Pr_Mobile");
			multiScreens.multiScreenShot(driver);

		}

		// Contact person 1 - Firstname verification
		String Act_sc1_FN = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[3]/div[2]/div[3]"))
				.getText();
		String Exp_sc1_FN = "Firstname is required.";

		try {
			Assert.assertEquals(Exp_sc1_FN, Act_sc1_FN);
			test.log(LogStatus.PASS,
					"Secondary Person First name field is mandate and Error message matching with expected" + "\""
							+ Act_sc1_FN + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Secondary Person First name error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_sc1_FN +\"Expected Result: \" + Exp_sc1_FN");
			multiScreens.multiScreenShot(driver);

		}

		// Contact person 1 - Lastname verification
		String Act_sc1_LN = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[3]/div[3]/div[3]"))
				.getText();
		String Exp_sc1_LN = "Lastname is required.";

		try {
			Assert.assertEquals(Exp_sc1_LN, Act_sc1_LN);
			test.log(LogStatus.PASS,
					"Secondary Person Last name field is mandate and Error message matching with expected" + "\""
							+ Act_sc1_FN + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Secondary Person Last name error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_sc1_FN +\"Expected Result: \" + Exp_sc1_FN");
			multiScreens.multiScreenShot(driver);

		}

		// Contact person 1 - Designation verification
		String Act_sc1_Desig = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[3]/div[4]/div/div[3]"))
				.getText();
		String Exp_sc1_Desig = "Designation is required.";

		try {
			Assert.assertEquals(Exp_sc1_Desig, Act_sc1_Desig);
			test.log(LogStatus.PASS,
					"Secondary Person Designation field is mandate and Error message matching with expected" + "\""
							+ Act_sc1_Desig + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Secondary Person Designation error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_sc1_Desig +\"Expected Result: \" + Exp_sc1_Desig");
			multiScreens.multiScreenShot(driver);

		}

		// Contact person 1 - Secondary Phone verification
		String Act_sc1_Ph_no = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[1]/div[3]"))
				.getText();
		String Exp_sc1_Ph_no = "Phone is required.";

		try {
			Assert.assertEquals(Exp_sc1_Ph_no, Act_sc1_Ph_no);
			test.log(LogStatus.PASS,
					"Secondary Person Phone number field is mandate and Error message matching with expected" + "\""
							+ Act_sc1_Ph_no + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Secondary Person Phone number error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_sc1_Ph_no +\"Expected Result: \" + Exp_sc1_Ph_no");
			multiScreens.multiScreenShot(driver);

		}

		// Contact person 1 - Secondary mobile verification
		String Act_sc1_mob_no = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[2]/div[3]"))
				.getText();
		String Exp_sc1_mob_no = "Mobile is required.";
		try {
			Assert.assertEquals(Exp_sc1_mob_no, Act_sc1_mob_no);
			test.log(LogStatus.PASS,
					"Secondary Person mobile number field is mandate and Error message matching with expected" + "\""
							+ Act_sc1_mob_no + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Secondary Person mobile number error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_sc1_mob_no +\"Expected Result: \" + Exp_sc1_mob_no");
			multiScreens.multiScreenShot(driver);

		}

		// Contact person 1 - Secondary mail verification
		String Act_sc1_mail = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[3]/div[3]"))
				.getText();
		String Exp_sc1_mail = "Email is required.";
		try {
			Assert.assertEquals(Exp_sc1_mail, Act_sc1_mail);
			test.log(LogStatus.PASS,
					"Secondary Person mail ID field is mandate and Error message matching with expected" + "\""
							+ Act_sc1_mail + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Secondary mail ID error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_sc1_mail +\"Expected Result: \" + Exp_sc1_mail");
			multiScreens.multiScreenShot(driver);

		}

		// Contact person 1 - Role selection verification
		String Act_sc1_role = driver
				.findElement(By
						.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[6]"))
				.getText();
		String Exp_sc1_role = "Role is required.";

		try {
			Assert.assertEquals(Exp_sc1_role, Act_sc1_role);
			test.log(LogStatus.PASS, "Secondary Person Role field is mandate and Error message matching with expected"
					+ "\"" + Act_sc1_role + "\"");
			multiScreens.multiScreenShot(driver);

		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either Secondary Role error message is not matching or the field is not marked as mandate.\" + \"Actual Result: \" + Act_sc1_role +\"Expected Result: \" + Exp_sc1_role");
			multiScreens.multiScreenShot(driver);

		}

		// Entering values for all fields
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		WebElement Primary_mobile = driver
				.findElement(By.xpath("//input[contains(@placeholder,'Enter mobile number')]"));
		Primary_mobile.sendKeys("904723289"); // Primary mobile
		WebElement Primary_email = driver.findElement(By.xpath("//input[contains(@placeholder,'example@email.com')]"));
		Primary_email.sendKeys("example@email.com"); // Primary email

		driver.switchTo().parentFrame(); // Parent Frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,400)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation

		driver.findElement(By.xpath("//*[(text()='Select')]")).click();
		driver.findElement(By.xpath("//*[(text()='Mrs.')]")).click();
		driver.findElement(By.xpath("//input[contains(@placeholder,'Enter first name')]")).sendKeys("Josy");
		driver.findElement(By.xpath("//input[contains(@placeholder,'Enter last name')]")).sendKeys("Vimal");
		driver.findElement(By.xpath("//*[(text()='--select designation--')]")).click();
		;
		driver.findElement(By.xpath("//*[(text()='Manager')]")).click(); //// Designation
		WebElement Sec1_ph_no = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter phone number')]"));
		Sec1_ph_no.sendKeys("904723289"); // Sec phone no.
		WebElement Sec1_mob_no = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[2]/div[2]/input"));
		Sec1_mob_no.sendKeys("904723289"); // Sec mobile number
		WebElement Sec1_email = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[3]/div[2]/input"));
		Sec1_email.sendKeys("example@email.com"); // Secondary email
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[5]/div[2]/div[2]/input"))
				.click(); // Assign role

		multiScreens.multiScreenShot(driver);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div/div/div[3]")).click(); // Next
																													// button

		// Duplicate mobile
		String Pri_mobile_check = driver.findElement(By
				.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[1]/div/div/div[2]/div[1]/div[2]/input"))
				.getText();
		String Sec1_mobile_check = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[2]/div[2]/input"))
				.getText();

		try {
			Assert.assertEquals(Pri_mobile_check, Sec1_mobile_check);
			String Act_mob_val = driver
					.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div/div/div[1]"))
					.getText();
			String Exp_mob_val = "Duplicate entries are not allowed.";
			try {
				Assert.assertEquals(Exp_mob_val, Act_mob_val);
				test.log(LogStatus.PASS,
						"If Primary mobile number and secondary mobile are same, system wont allow to proceed and throw following error message: "
								+ "\"" + Act_mob_val + "\"");
				multiScreens.multiScreenShot(driver);
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Error message is not matching, Expected message: " + "\"" + Exp_mob_val + "\""
						+ "but actual message was, " + "\"" + Act_mob_val + "\"");
			}
		} catch (AssertionError e) {

		}

		// Duplicate mail ID
		String Pri_mail_check = driver.findElement(By
				.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[1]/div/div/div[2]/div[2]/div[2]/input"))
				.getText();
		String Sec1_mail_check = driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[3]/div[2]/input"))
				.getText();

		try {
			Assert.assertEquals(Pri_mail_check, Sec1_mail_check);
			String Act_mail_val = driver
					.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div/div/div[1]"))
					.getText();
			String Exp_mail_val = "Duplicate entries are not allowed.";
			try {

				Assert.assertEquals(Exp_mail_val, Act_mail_val);
				test.log(LogStatus.PASS,
						"If Primary mail ID and secondary mail ID are same, system wont allow to proceed and throw following error message: "
								+ "\"" + Act_mail_val + "\"");
				multiScreens.multiScreenShot(driver);
			} catch (AssertionError e) {
				test.log(LogStatus.FAIL, "Error message is not matching, Expected message: " + "\"" + Exp_mail_val
						+ "\"" + "but actual message was, " + "\"" + Act_mail_val + "\"");
				multiScreens.multiScreenShot(driver);
			}
		} catch (AssertionError e) {
		}

		// Mobile number field validation

		// Entering values for all fields
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[2]/div[2]/input"))
				.clear();
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[2]/div[2]/input"))
				.sendKeys("904723280"); // Sec mobile number
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[3]/div[2]/input"))
				.clear();
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[3]/div[2]/input"))
				.sendKeys("example007@email.com"); // Secondary email

		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div/div/div[3]")).click(); // Next
																													// button
	}

	@Test(priority = 5)
	void photo_gallery() throws InterruptedException, AWTException, IOException {
		// Photo Gallery
		test = report.startTest("Validation of Photo Gallery");
		try {
			driver.switchTo().parentFrame(); // Parent Frame
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// Navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[3]")).click(); // Navigation to Photo
																								// Gallery Tab
			test.log(LogStatus.PASS, "Photo Gallery Tab loading properly");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Photo Gallery Tab is not loading properly");
			multiScreens.multiScreenShot(driver);
		}

		// Proceeding 'Next' without entering any values
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[4]/div/div/div[2]")).click(); // Next
																													// Button
		Thread.sleep(3000);

		// Thumbnail image validation (2 images)
		String Act_message = driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[3]")).getText();
		String Exp_message = "Thumbnail is required, Please upload atleast 2 images.";
		try {
			Assert.assertEquals(Exp_message, Act_message);
			test.log(LogStatus.PASS,
					"Since minimum required image is 2, same is not satified. So following alert message thrown:" + "\""
							+ Act_message + "\"");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either minimum requirement satisfaction is satified or error message is not displayed properly.");
			multiScreens.multiScreenShot(driver);
		}

		// Thumbnail image validation (5 images)
		String Act_message1 = driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div/div/div[2]/div[4]"))
				.getText();
		String Exp_message1 = "Image is required, Please upload atleast 5 images.";
		try {
			Assert.assertEquals(Exp_message1, Act_message1);
			test.log(LogStatus.PASS,
					"Since minimum required image is 5, same is not satified. So following alert message thrown:" + "\""
							+ Act_message1 + "\"");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL,
					"Either minimum requirement satisfaction is satified or error message is not displayed properly.");
			multiScreens.multiScreenShot(driver);
		}

		// Uploading all images.
		// Image 1
		WebElement Imgupl = driver.findElement(By
				.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[2]/div[1]/div[2]/div[1]/div"));
		Imgupl.click();
		Thread.sleep(3000);
		Robot image1 = new Robot();
		// copying File path to Clipboard
		StringSelection str1 = new StringSelection(
				"C:\\Users\\avimalraj\\eclipse-workspace\\Enroll_Hotel\\Images for testing upload\\783px-Test-Logo.svg.png");

		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str1, null);
		// press Contol+V for pasting
		image1.keyPress(KeyEvent.VK_CONTROL);
		image1.keyPress(KeyEvent.VK_V);

		// release Contol+V for pasting
		image1.keyRelease(KeyEvent.VK_CONTROL);
		image1.keyRelease(KeyEvent.VK_V);

		// for pressing and releasing Enter
		image1.keyPress(KeyEvent.VK_ENTER);
		image1.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		multiScreens.multiScreenShot(driver);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		driver.switchTo().activeElement().findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div/div/div[3]/div[2]"))
				.click();
		Thread.sleep(3000);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		multiScreens.multiScreenShot(driver);

		// 2nd image from 2 list
		WebElement Imgupl2 = driver.findElement(By
				.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div"));
		Imgupl2.click();
		Thread.sleep(3000);
		Robot image2 = new Robot();
		// copying File path to Clipboard
		StringSelection str2 = new StringSelection("C:\\Automation\\Images\\img1.jpg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str2, null);
		// press Contol+V for pasting
		image2.keyPress(KeyEvent.VK_CONTROL);
		image2.keyPress(KeyEvent.VK_V);
		// release Contol+V for pasting
		image2.keyRelease(KeyEvent.VK_CONTROL);
		image2.keyRelease(KeyEvent.VK_V);
		// for pressing and releasing Enter
		image2.keyPress(KeyEvent.VK_ENTER);
		image2.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		driver.switchTo().activeElement().findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div/div[3]/div[2]"))
				.click();
		multiScreens.multiScreenShot(driver);

		// 1st image from 5 list
		Thread.sleep(3000);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,700)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		WebElement Imgupl3 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
		Imgupl3.click();
		Thread.sleep(3000);
		Robot image3 = new Robot();
		// copying File path to Clipboard
		StringSelection str3 = new StringSelection(
				"C:\\Users\\avimalraj\\eclipse-workspace\\Enroll_Hotel\\Images for testing upload\\783px-Test-Logo.svg.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str3, null);

		// press Contol+V for pasting
		image3.keyPress(KeyEvent.VK_CONTROL);
		image3.keyPress(KeyEvent.VK_V);

		// release Contol+V for pasting
		image3.keyRelease(KeyEvent.VK_CONTROL);
		image3.keyRelease(KeyEvent.VK_V);
		// for pressing and releasing Enter
		image3.keyPress(KeyEvent.VK_ENTER);
		image3.keyRelease(KeyEvent.VK_ENTER);
		multiScreens.multiScreenShot(driver);

		// 2nd image from 5 list
		Thread.sleep(3000);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,700)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		WebElement Imgupl4 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
		Imgupl4.click();
		Thread.sleep(3000);
		Robot image4 = new Robot();
		// copying File path to Clipboard
		StringSelection str4 = new StringSelection(
				"C:\\Users\\avimalraj\\eclipse-workspace\\Enroll_Hotel\\Images for testing upload\\783px-Test-Logo.svg.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str4, null);

		// press Contol+V for pasting
		image4.keyPress(KeyEvent.VK_CONTROL);
		image4.keyPress(KeyEvent.VK_V);
		// release Contol+V for pasting
		image4.keyRelease(KeyEvent.VK_CONTROL);
		image4.keyRelease(KeyEvent.VK_V);
		// for pressing and releasing Enter
		image4.keyPress(KeyEvent.VK_ENTER);
		image4.keyRelease(KeyEvent.VK_ENTER);
		multiScreens.multiScreenShot(driver);

		// 3rd image from 5 list
		Thread.sleep(3000);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,700)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		WebElement Imgupl5 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
		Imgupl5.click();
		Thread.sleep(3000);

		Robot image5 = new Robot();
		// copying File path to Clipboard
		StringSelection str5 = new StringSelection("C:\\Automation\\Images\\img1.jpg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str5, null);
		// press Contol+V for pasting
		image5.keyPress(KeyEvent.VK_CONTROL);
		image5.keyPress(KeyEvent.VK_V);
		// release Contol+V for pasting
		image5.keyRelease(KeyEvent.VK_CONTROL);
		image5.keyRelease(KeyEvent.VK_V);
		// for pressing and releasing Enter
		image5.keyPress(KeyEvent.VK_ENTER);
		image5.keyRelease(KeyEvent.VK_ENTER);
		multiScreens.multiScreenShot(driver);

		// 4th image from 5 list

		Thread.sleep(3000);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,700)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		WebElement Imgupl6 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
		Imgupl6.click();
		Thread.sleep(3000);
		Robot image6 = new Robot();
		// copying File path to Clipboard
		StringSelection str6 = new StringSelection(
				"C:\\Users\\avimalraj\\eclipse-workspace\\Enroll_Hotel\\Images for testing upload\\download.jfif");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str6, null);

		// press Contol+V for pasting
		image6.keyPress(KeyEvent.VK_CONTROL);
		image6.keyPress(KeyEvent.VK_V);

		// release Contol+V for pasting
		image6.keyRelease(KeyEvent.VK_CONTROL);
		image6.keyRelease(KeyEvent.VK_V);

		// for pressing and releasing Enter
		image6.keyPress(KeyEvent.VK_ENTER);
		image6.keyRelease(KeyEvent.VK_ENTER);
		multiScreens.multiScreenShot(driver);

		// 5th image from 5 list
		Thread.sleep(3000);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,700)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		WebElement Imgupl7 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
		Imgupl7.click();
		Thread.sleep(3000);
		Robot image7 = new Robot();
		// copying File path to Clipboard
		StringSelection str7 = new StringSelection(
				"C:\\Users\\avimalraj\\eclipse-workspace\\Enroll_Hotel\\Images for testing upload\\download.jfif");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str7, null);

		// press Contol+V for pasting
		image7.keyPress(KeyEvent.VK_CONTROL);
		image7.keyPress(KeyEvent.VK_V);

		// release Contol+V for pasting
		image7.keyRelease(KeyEvent.VK_CONTROL);
		image7.keyRelease(KeyEvent.VK_V);

		// for pressing and releasing Enter
		image7.keyPress(KeyEvent.VK_ENTER);
		image7.keyRelease(KeyEvent.VK_ENTER);
		multiScreens.multiScreenShot(driver);
	}

//Room type tab validation
	@Test(priority = 6)
	void room_type() throws InterruptedException, IOException, AWTException {
		test = report.startTest("Room type tab validation");
		try {
			driver.switchTo().parentFrame(); // Parent Frame
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// navigation

			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[4]")).click(); // Navigation to Room Type
																								// Tab
			test.log(LogStatus.PASS, "Room type tab is loading properly");
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Room type tab is not loading properly");
		}

		// Proceeding 'Next' without entering any values
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/wrapper/div[2]/div[2]"))
				.click(); // Next Button
		Thread.sleep(3000);
		multiScreens.multiScreenShot(driver);
		// Room type validation
		String Act_room_type = driver
				.findElement(By
						.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[1]/div[1]/div/div[3]"))
				.getText();
		String Exp_room_type = "Title is required.";
		try {
			Assert.assertEquals(Exp_room_type, Act_room_type);
			test.log(LogStatus.PASS,
					"Room type field is mandate, since the value is not available the following error throwing: "
							+ Act_room_type);
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Room type field available or error message is incorrect.");
			multiScreens.multiScreenShot(driver);
		}

		// Travellor category validation
		String Act_tr_cat = driver
				.findElement(By
						.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[1]/div[2]/div/div[3]"))
				.getText();
		String Exp_tr_cat = "Category is required.";
		try {
			Assert.assertEquals(Exp_tr_cat, Act_tr_cat);
			test.log(LogStatus.PASS,
					"Travellor category field is mandate, since the value is not available the following error throwing: "
							+ Act_tr_cat);
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Travellor category field available or error message is incorrect.");
			multiScreens.multiScreenShot(driver);
		}

		// No. of rooms available in property
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,350)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		String Act_rooms = driver
				.findElement(By
						.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[3]/div[1]/div/div[3]"))
				.getText();
		String Exp_rooms = "Number of rooms is required.";
		try {
			Assert.assertEquals(Exp_rooms, Act_rooms);
			test.log(LogStatus.PASS,
					"Number of rooms field is mandate, since the value is not available the following error throwing: "
							+ Act_rooms);
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either No. of rooms field available or error message is incorrect.");
			multiScreens.multiScreenShot(driver);
		}

		// No. of rooms in namlatic
		String Act_nam_rooms = driver
				.findElement(By
						.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[3]/div[1]/div/div[3]"))
				.getText();
		String Exp_nam_rooms = "Number of rooms is required.";
		try {
			Assert.assertEquals(Exp_nam_rooms, Act_nam_rooms);
			test.log(LogStatus.PASS,
					"Number of rooms field is mandate, since the value is not available the following error throwing: "
							+ Act_nam_rooms);
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either No. of rooms field available or error message is incorrect.");
			multiScreens.multiScreenShot(driver);
		}

		// Room Sharing
		String Act_sharing = driver
				.findElement(By
						.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[3]"))
				.getText();
		String Exp_sharing = "Sharing is required, Please add at least one sharing type.";
		try {
			Assert.assertEquals(Exp_sharing, Act_sharing);
			test.log(LogStatus.PASS,
					"Room sharing field is mandate, since the value is not available the following error throwing: "
							+ Act_sharing);
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either room sharing field available or error message is incorrect.");
			multiScreens.multiScreenShot(driver);
		}

		// Amenities verification
		String Act_amenities = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[6]/div/div/div[3]"))
				.getText();
		String Exp_amenities = "Amenities is required, Please choose an amenity.";
		try {
			Assert.assertEquals(Exp_amenities, Act_amenities);
			test.log(LogStatus.PASS,
					"Amenities field is mandate, since the value is not available the following error throwing: "
							+ Act_amenities);
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Amenities field available or error message is incorrect.");
			multiScreens.multiScreenShot(driver);
		}

		// Photo upload verification
		String Act_photo = driver
				.findElement(By.xpath(
						"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[7]/div/div/div[2]/div[3]"))
				.getText();
		String Exp_photo = "Image is required, Please upload atleast 2 images.";
		try {
			Assert.assertEquals(Exp_photo, Act_photo);
			test.log(LogStatus.PASS,
					"Minimum two images are mandate, since the required input is not available so the following error throwing: "
							+ Act_photo);
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Amenities field available or error message is incorrect.");
			multiScreens.multiScreenShot(driver);
		}
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		// Entering all values
		driver.findElement(By.xpath("//*[contains(@placeholder,'--Select room type--')]")).click();
		driver.findElement(By.xpath("//*[text()='Classic']")).click();

		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,350)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[contains(@placeholder,'--Select room--')]")).click();
		WebElement avl_count = driver.findElement(By.xpath("//*[text()='99']"));
		avl_count.click();

		driver.findElement(By.xpath("//*[contains(@placeholder,'--Select room--')]")).click();
		WebElement count = driver.findElement(By.xpath("//*[text()='100']"));
		count.click();

		// Sharing 1
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[1]/div/div[1]/div/input"))
				.click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[1]/div/div[4]/div[2]/input[1]"))
				.sendKeys("25.23");
		Thread.sleep(2000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[1]/div/div[4]/div[3]/input[1]"))
				.sendKeys("60.58");
		Thread.sleep(2000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[1]/div/div[4]/div[4]/input[1]"))
				.sendKeys("3410.12");
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);

		// Sharing 2
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[2]/div/div[1]/div/input"))
				.click();
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[2]/div/div[4]/div[2]/input[1]"))
				.sendKeys("25.23");
		Thread.sleep(2000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[2]/div/div[4]/div[3]/input[1]"))
				.sendKeys("60.58");
		Thread.sleep(2000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[2]/div/div[4]/div[4]/input[1]"))
				.sendKeys("3410.12");
		Thread.sleep(2000);

		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		Thread.sleep(3000);
		// ((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation

		// Selection of amenities
		driver.findElement(By.xpath("//*[@placeholder='--Select amenities--']")).click();
		driver.findElement(By.xpath("//*[text()='Room Service']")).click();

		driver.switchTo().defaultContent();
		multiScreens.multiScreenShot(driver);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,900)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		multiScreens.multiScreenShot(driver);

		// Sharing 3
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,800)");
		Thread.sleep(3000);

		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[3]/div/div[1]/div/input"))
				.click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[3]/div/div[4]/div[2]/input[1]"))
				.sendKeys("30.89");
		Thread.sleep(2000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[3]/div/div[4]/div[3]/input[1]"))
				.sendKeys("60.58");
		Thread.sleep(2000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[3]/div/div[4]/div[4]/input[1]"))
				.sendKeys("3410.12");
		Thread.sleep(2000);
		multiScreens.multiScreenShot(driver);

		// Selection of Popular filter
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,900)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@placeholder='--Select popular filter--']")).click();
		driver.findElement(By.xpath("//*[text()='Couple friendly']")).click();

		multiScreens.multiScreenShot(driver);
		driver.switchTo().defaultContent();
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,50)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		// Travellor category.
		driver.findElement(By.xpath("//*[contains(@placeholder,'--Select traveller category--')]")).click();
		driver.findElement(By.xpath("//*[text()='Couple']")).click();

		multiScreens.multiScreenShot(driver);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1100)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		// Photo upload
		WebElement roomimgupl1 = driver.findElement(By.xpath("//*[text()='Click to upload']"));
		roomimgupl1.click();
		Thread.sleep(3000);
		Robot roomrb1 = new Robot();
		// copying File path to Clipboard
		StringSelection roomstr1 = new StringSelection("C:\\Automation\\Images\\img1.jpg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(roomstr1, null);
		// press Control+V for pasting
		roomrb1.keyPress(KeyEvent.VK_CONTROL);
		roomrb1.keyPress(KeyEvent.VK_V);
		// release Control+V for pasting
		roomrb1.keyRelease(KeyEvent.VK_CONTROL);
		roomrb1.keyRelease(KeyEvent.VK_V);
		// for pressing and releasing Enter
		roomrb1.keyPress(KeyEvent.VK_ENTER);
		roomrb1.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);

		// 2nd Image upload
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1200)");
		Thread.sleep(3000);
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		WebElement roomimgupl2 = driver.findElement(By.xpath("//*[text()='Click to upload']"));
		roomimgupl2.click();
		Thread.sleep(3000);
		Robot roomrb2 = new Robot();
		// copying File path to Clipboard
		StringSelection roomstr2 = new StringSelection("C:\\Automation\\Images\\img1.jpg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(roomstr2, null);
		// press Control+V for pasting
		roomrb2.keyPress(KeyEvent.VK_CONTROL);
		roomrb2.keyPress(KeyEvent.VK_V);
		// release Control+V for pasting
		roomrb2.keyRelease(KeyEvent.VK_CONTROL);
		roomrb2.keyRelease(KeyEvent.VK_V);
		// for pressing and releasing Enter
		roomrb2.keyPress(KeyEvent.VK_ENTER);
		roomrb2.keyRelease(KeyEvent.VK_ENTER);
		multiScreens.multiScreenShot(driver);

		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/wrapper/div[2]/div[2]"))
				.click(); // Next button

		// Error validation if namlatic count is higher than available count
		String Act_count_diff = driver
				.findElement(By
						.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[3]/div[2]/div/div[3]"))
				.getText();
		String Exp_count_diff = "Number of rooms register should be less than total number of rooms available in this type at property";
		try {
			Assert.assertEquals(Exp_count_diff, Act_count_diff);
			test.log(LogStatus.PASS,
					"Namlatic count should not be more than Avalaible room count, since it is more the following error message triggered. "
							+ "\"" + Act_count_diff + "\"");
			multiScreens.multiScreenShot(driver);
			driver.switchTo().parentFrame(); // Parent frame navigation
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,350)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// navigation
			Thread.sleep(3000);

			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[3]/div[2]/div/div[2]/div/div/div[1]"))
					.click();
			WebElement nam_count1 = driver.findElement(By.xpath("//*[text()='95']"));
			nam_count1.click();

			driver.switchTo().parentFrame(); // Parent frame navigation
			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// navigation
			Thread.sleep(3000);
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/wrapper/div[2]/div[2]")).click(); // Next
																														// button
		} catch (AssertionError e) {
			test.log(LogStatus.PASS,
					"Either Namlatic count is not exceeds Avl.count or the alert message displayed is incorrect.");
			multiScreens.multiScreenShot(driver);
		}

		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		// driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/wrapper/div[2]/div[2]")).click();
		// // Next button
	}

	// Social media validation
	@Test(priority = 7)
	void social_media() throws InterruptedException, IOException, AWTException {
		test = report.startTest("Social media tab verification");
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");

		try {
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																													// navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[5]")).click(); // Navigation to Social
																								// Type Tab
			multiScreens.multiScreenShot(driver);
			test.log(LogStatus.PASS, "Social media tab is loading properly");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Social media tab is not loading properly");
			multiScreens.multiScreenShot(driver);
		}

		// Entering incorrect value
		driver.findElement(By.xpath("//*[@placeholder='Enter Facebook URL']")).sendKeys("Example1");
		driver.findElement(By.xpath("//*[@placeholder='Enter Twitter URL']")).sendKeys("Example2");
		driver.findElement(By.xpath("//*[@placeholder='Enter Instagram URL']")).sendKeys("Example3");
		driver.findElement(By.xpath("//*[@placeholder='Enter Linkedin URL']")).sendKeys("Example4");
		driver.findElement(By.xpath("//*[@placeholder='Enter Youtube URL']")).sendKeys("Example5");

		multiScreens.multiScreenShot(driver);

		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[6]/div[2]/div")).click(); // Next

		// Error validation
		String Act_FB_error = driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[1]/div[2]/div[2]"))
				.getText();
		String Exp_FB_error = "Facebook is not valid";
		try {

			Assert.assertEquals(Exp_FB_error, Act_FB_error);

			test.log(LogStatus.PASS, "Facebook URL is incorrect");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Facebook URL is correct or alert message is incorrect");
			multiScreens.multiScreenShot(driver);
		}

		String Act_TW_error = driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[2]/div[2]/div[2]"))
				.getText();
		String Exp_TW_error = "Twitter is not valid";
		try {

			Assert.assertEquals(Exp_TW_error, Act_TW_error);

			test.log(LogStatus.PASS, "Twitter URL is incorrect");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Twitter URL is correct or alert message is incorrect");
			multiScreens.multiScreenShot(driver);
		}

		String Act_IG_error = driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[3]/div[2]/div[2]"))
				.getText();
		String Exp_IG_error = "Instagram is not valid";
		try {

			Assert.assertEquals(Exp_IG_error, Act_IG_error);

			test.log(LogStatus.PASS, "Instagram URL is incorrect");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Instagram URL is correct or alert message is incorrect");
			multiScreens.multiScreenShot(driver);
		}

		String Act_LN_error = driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[4]/div[2]/div[2]"))
				.getText();
		String Exp_LN_error = "Linkedin is not valid";
		try {

			Assert.assertEquals(Exp_LN_error, Act_LN_error);

			test.log(LogStatus.PASS, "LinkedIn URL is incorrect");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either LinkedIn URL is correct or alert message is incorrect");
			multiScreens.multiScreenShot(driver);
		}

		String Act_YT_error = driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[5]/div[2]/div[2]"))
				.getText();
		String Exp_YT_error = "Youtube is not valid";
		try {

			Assert.assertEquals(Exp_YT_error, Act_YT_error);

			test.log(LogStatus.PASS, "Youtube URL is incorrect");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either Youtube URL is correct or alert message is incorrect");
			multiScreens.multiScreenShot(driver);
		}

		multiScreens.multiScreenShot(driver);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@placeholder='Enter Facebook URL']")).clear();
		driver.findElement(By.xpath("//*[@placeholder='Enter Facebook URL']")).sendKeys("Example1@newmail.com");
		driver.findElement(By.xpath("//*[@placeholder='Enter Twitter URL']")).clear();
		driver.findElement(By.xpath("//*[@placeholder='Enter Twitter URL']")).sendKeys("Example2@newmail.com");
		driver.findElement(By.xpath("//*[@placeholder='Enter Instagram URL']")).clear();
		driver.findElement(By.xpath("//*[@placeholder='Enter Instagram URL']")).sendKeys("Example3@newmail.com");
		driver.findElement(By.xpath("//*[@placeholder='Enter Linkedin URL']")).clear();
		driver.findElement(By.xpath("//*[@placeholder='Enter Linkedin URL']")).sendKeys("Example4@newmail.com");
		driver.findElement(By.xpath("//*[@placeholder='Enter Youtube URL']")).clear();
		driver.findElement(By.xpath("//*[@placeholder='Enter Youtube URL']")).sendKeys("Example5@newmail.com");
		multiScreens.multiScreenShot(driver);
		driver.switchTo().parentFrame(); // Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,1600)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[6]/div[2]/div")).click(); // Next
	}

	// Finance & Legal
	@Test(priority = 9)
	void finance() throws IOException, InterruptedException, AWTException {
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,0)");
		test = report.startTest("Finance & Legal tab verification");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[6]")).click(); // Navigation to Finance &
																							// Legal Tab

		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,900)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		driver.findElement(By.xpath("//*[text()='FINISH, REGISTER THIS PROPERTY']")).click();

		// driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[6]")).click();
		// //Navigation to Finance & Legal Tab

		// Error validation
		String Act_doc_Error = driver
				.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[1]/div[3]"))
				.getText();
		String Exp_doc_Error = "Document title is required";
		try {
			Assert.assertEquals(Exp_doc_Error, Act_doc_Error);
			test.log(LogStatus.PASS, "Document title is mandate");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either document title is available or error message is incorrect");
			multiScreens.multiScreenShot(driver);
		}

		String Act_date1_Error = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/div[3]"))
				.getText();
		String Exp_date1_Error = "Document created date is required";
		try {
			Assert.assertEquals(Exp_date1_Error, Act_date1_Error);
			test.log(LogStatus.PASS, "Document created date is mandate");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either document created date is available or error message is incorrect");
			multiScreens.multiScreenShot(driver);
		}

		String Act_date2_Error = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div[3]"))
				.getText();
		String Exp_date2_Error = "Document valid date is required";
		try {
			Assert.assertEquals(Exp_date2_Error, Act_date2_Error);
			test.log(LogStatus.PASS, "Document valid date is mandate");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either document valid date is available or error message is incorrect");
			multiScreens.multiScreenShot(driver);
		}

		String Act_doc_upload = driver
				.findElement(
						By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[2]/div/div[2]/div[4]"))
				.getText();
		String Exp_doc_upload = "Documents is required, Please upload the Document";
		try {
			Assert.assertEquals(Exp_doc_upload, Act_doc_upload);
			test.log(LogStatus.PASS, "Document upload is mandate");
			multiScreens.multiScreenShot(driver);
		} catch (AssertionError e) {
			test.log(LogStatus.FAIL, "Either document uploaded or error message is incorrect");
			multiScreens.multiScreenShot(driver);
		}

		// Entering values for all fields
		driver.findElement(By.xpath(
				" /html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div[1]/div[1]"))
				.click();
		driver.findElement(By.xpath("//*[text()='Companies register']")).click();

		// File Upload
		WebElement imgupl = driver.findElement(
				By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[2]/div/div[2]/div[2]/div/div"));
		imgupl.click();
		Thread.sleep(6000);
		Robot rb = new Robot();
		// copying File path to Clipboard
		StringSelection str = new StringSelection(
				"C:\\Users\\avimalraj\\eclipse-workspace\\Enroll_Hotel\\Images for testing upload\\783px-Test-Logo.svg.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
		// press Contol+V for pasting
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);
		// release Contol+V for pasting
		rb.keyRelease(KeyEvent.VK_CONTROL);
		rb.keyRelease(KeyEvent.VK_V);
		// for pressing and releasing Enter
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		multiScreens.multiScreenShot(driver);

		driver.findElement(By.xpath("//*[@placeholder='--Select--']")).click();
		driver.findElement(By.xpath("//*[text()='2']")).click();
		driver.findElement(By.xpath(
				"/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div/input"))
				.click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[text()='10']")).click();
		multiScreens.multiScreenShot(driver);
		driver.switchTo().parentFrame(); // Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("scroll(0,900)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame
																												// navigation
		driver.findElement(By.xpath("//*[text()='FINISH, REGISTER THIS PROPERTY']")).click();
	}

	@AfterMethod
	public static void endMethod() {
		report.endTest(test);
	}

	@AfterClass
	public static void endTest() {
		System.out.println("End");
		report.flush();
		report.close();
		// driver.quit();
	}

}