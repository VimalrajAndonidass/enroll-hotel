package Enroll_FE_French;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import multiScreenShot.MultiScreenShot;

public class PlaceholderText_Validation_FE_en {
	public static WebDriver driver = null;
	public static MultiScreenShot multiScreens = new MultiScreenShot("C:\\Automation\\Screenshots\\Enroll_Hotel\\FE\\","Enroll_Hotel_Placeholdertext_Validation");
	static ExtentTest test;
	static ExtentReports report;
	
	@BeforeTest
	public void declaration()
	{
		//Webdriver declaration
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/Chrome/chromedriver.exe");
		driver = new ChromeDriver();
		//Report configuration
			report = new ExtentReports(System.getProperty("user.dir")+"Enroll_ExtentReportResults.html");
		    report.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}

	@Test (priority = 0, invocationCount = 1)
	public void startTest() throws IOException
	{
		test = report.startTest("Enroll Hotel - Placeholder text Validation");
		try
		{
			test.log(LogStatus.PASS, "Enroll Hotel - Placeholder text Validation - English");
		} catch (AssertionError e)
		{
			
		}
	}
	@Test (priority = 1, invocationCount = 1)
	public void URL_Launch() throws IOException
	{
		test = report.startTest("URL Verification");
		driver.get("chrome://settings/clearBrowserData");
		driver.navigate().to("https://test.namlatic.com/");
		driver.manage().window().maximize();
		multiScreens.multiScreenShot(driver);	
		
		try {
			Assert.assertEquals("https://test.namlatic.com/", "https://test.namlatic.com/");
			test.log(LogStatus.PASS, "URL matching - Pass");
		} catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "URL mismatching - Fail");
		}
	}
	
	@Test (priority =2, invocationCount = 1)
	//Language change
	public void lang_change() throws IOException
	{
		test = report.startTest("Language Change");
		try
		{
			WebElement language = driver.findElement(By.xpath("//*[(text()='Fran�ais')]"));
			language.click();
			WebElement chglang = language.findElement(By.xpath("//*[(text()='English')]"));
			chglang.click();
			test.log(LogStatus.PASS, "English language changed successfully");
		}
		catch(AssertionError e)
		{
			test.log(LogStatus.PASS, "English language not changed");
		}
	}
	@Test (priority = 3, invocationCount = 1)	
	public void login() throws InterruptedException, AWTException, IOException
	{
		test = report.startTest("Login Verification");
		try
		{
		WebElement nam_Login = driver.findElement(By.xpath("//*[(text()='Login')]"));
		nam_Login.click();
		WebElement uname = driver.findElement(By.xpath("//input[contains(@type,'text')]")); 	
		uname.sendKeys("testt62@yopmail.com");
		WebElement pwd = driver.findElement(By.xpath("//input[contains(@type,'password')]")); 	
		pwd.sendKeys("Test123456");
		multiScreens.multiScreenShot(driver);
		pwd.sendKeys(Keys.ENTER);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		Thread.sleep(2000);
		test.log(LogStatus.PASS, "Login Pass");
		}
		catch (AssertionError e)
		{
			test.log(LogStatus.FAIL, "Login Failed");
		}
	}

	@Test(priority = 4, invocationCount = 1)
	void Enrol() throws InterruptedException, AWTException, IOException 
 		{
		test = report.startTest("Enroll Hotel Button status");
		try
		{
		Thread.sleep(3000);
	 	WebElement enhotel = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div/div[1]/div[1]/div"));
	 	System.out.println(enhotel.getText());
	 	enhotel.click();
	 	multiScreens.multiScreenShot(driver);	
	 	test.log(LogStatus.PASS, "Enroll Hotel selected successfully");
		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Unable to select Enroll hotel");		
		}
 
 }
//Welcome page text verification
	@Test (priority = 5, invocationCount = 1)
	public void Welcome_page_text_Verification() throws IOException, InterruptedException
	{
		test = report.startTest("Welcome Page Verification");	
		try
		{
		
		String Exptext1 = "Welcome to Namlatic";
		String Acttext1 = (driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[1]/div")).getText());
		AssertJUnit.assertEquals(Exptext1, Acttext1);
	//Line 1
		String Exptext2 ="It�s free to enroll your hotel.";
		String Acttext2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[2]/div")).getText();
		AssertJUnit.assertEquals(Exptext2, Acttext2);
	//Line 2
		String Exptext3 ="12/5 support by phone or email.";
		String Acttext3= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]/div")).getText();
		AssertJUnit.assertEquals(Exptext3, Acttext3);
	//Line 3
		String Exptext4 ="Access the site with 3 language (Arabic, English & French)";
		String Acttext4= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[4]/div")).getText();
		AssertJUnit.assertEquals(Exptext4, Acttext4);
	//Line 4
		String Exptext5 ="Set your own prices with 3 different currencies (EUR, DZD & USD)";
		String Acttext5= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[5]/div")).getText();
		AssertJUnit.assertEquals(Exptext5, Acttext5);
		test.log(LogStatus.PASS, "Welcome Page text verified, Pass");
		System.out.println("Welcome Page text verified, Pass");
		}
		catch (AssertionError e)
		  {
			System.out.println("Welcome page text mismatches with expected");
			test.log(LogStatus.FAIL, "Welcome page text mismatches with expected");
		  }
	//Ensure the availability of Get started button
	try {
		//driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[2]/div/div/div[4]/div/div/div[2]/div")).isDisplayed();
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[6]/div/div")).isEnabled();
		System.out.println("Get started button is available and allowed to select");
		test.log(LogStatus.PASS, "Get started button is available and allowed to select");
		} catch (Exception e)
		{
			System.out.println("Get started button is not available or not able to selectable");
			test.log(LogStatus.FAIL, "Get Started button either not available or not able to selectable");
		}
	//Click on get started button
	try
	{
		WebElement getstarted = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[6]/div/div"));
	 	getstarted.click();
	 	multiScreens.multiScreenShot(driver);
	 	Thread.sleep(3000);
	 	test.log(LogStatus.PASS, "Get started button pass to navigate add hotel screen");
	}
	catch(Exception e)
	{
		System.out.println("Get started button is not clickable");
		test.log(LogStatus.FAIL, "Get started button is not clickable");
	}
	
}
	@Test(priority = 6, invocationCount=1)
	void basic_details() throws IOException, InterruptedException
		{
		test = report.startTest("Basic details Tab verification");
		Thread.sleep(3000);
		// Add hotel details title
		driver.switchTo().parentFrame(); //Parent Frame
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame Navigation
 		Thread.sleep(3000);
//Placeholder text verification at Basic info tab
 		
 		String exp_title = "Add hotel details";
		String act_title =driver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div[1]")).getText();
	
		try {
			Assert.assertEquals(exp_title, act_title);	
			test.log(LogStatus.PASS, "Add hotel details title available");
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Add hotel details title is not available");
			}
		multiScreens.multiScreenShot(driver);
//Status: Not published
		String exp_status = "Status: Not published";
		String act_status =driver.findElement(By.xpath(" /html/body/div/div/div/div[2]/div[2]")).getText();
		
		try {
			Assert.assertEquals(exp_status, act_status);
			test.log(LogStatus.PASS, "Not published status available");
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Not published status is not available");
			}
		multiScreens.multiScreenShot(driver);
		
		//Basic Info
		try
		{
 		test.log(LogStatus.PASS, "Basic details Tab loading properly");
		multiScreens.multiScreenShot(driver);
 		
		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Basic details Tab not loading properly");
		multiScreens.multiScreenShot(driver);
		}
	
	// Check Enter property details title
		driver.switchTo().parentFrame(); //Parent Frame
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame Navigation
 		Thread.sleep(3000);
 		
 		String Exp_Property_det = "Enter Property details";
 		String Act_Property_det = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[1]/div"))).getText();
		try
		{
		Assert.assertEquals(Exp_Property_det, Act_Property_det);
		test.log(LogStatus.PASS, "Property details title is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Property details title is not available");	
		multiScreens.multiScreenShot(driver);
		}
 		//Check source text
		String Exp_source_text = "Source";
 		String Act_source_text = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[1]/div[1]"))).getText();
		try
		{
		Assert.assertEquals(Exp_source_text, Act_source_text);
		test.log(LogStatus.PASS, "Source text is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Source text is not available");	
		multiScreens.multiScreenShot(driver);
		}
		//Check Target text
				String Exp_target_text = "Target";
		 		String Act_target_text = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[2]/div[1]"))).getText();
				try
				{
				Assert.assertEquals(Exp_target_text, Act_target_text);
				test.log(LogStatus.PASS, "Target text is available" );
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Target text is not available");	
				multiScreens.multiScreenShot(driver);
				}
		//Ensure Translate button
				String Exp_translate_btn = "Translate";
		 		String Act_translate_btn = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[3]/div"))).getText();
				try
				{
				Assert.assertEquals(Exp_translate_btn, Act_translate_btn);
				test.log(LogStatus.PASS, "Translate button is available" );
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Translate button is not available");	
				multiScreens.multiScreenShot(driver);
				}
			
				
 	//Check Property name title
 	
 		String Exp_Property_title = "Enter Property Name";
 		String Act_Property_title = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div/div[1]/div[1]	"))).getText();
		try
		{
		Assert.assertEquals(Exp_Property_title, Act_Property_title);
		test.log(LogStatus.PASS, "Property name title is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Property name title is not available");	
		multiScreens.multiScreenShot(driver);
		}
		
		// Check EN button
		String Exp_en_btn = "EN";
 		String Act_en_btn = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div/div[1]/div[2]/div[1]/div"))).getText();
		try
		{
		Assert.assertEquals(Exp_en_btn, Act_en_btn);
		test.log(LogStatus.PASS, "EN button is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "EN button is not available");	
		multiScreens.multiScreenShot(driver);
		}
		// Check FR button
				String Exp_fr_btn = "FR";
		 		String Act_fr_btn = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div/div[1]/div[2]/div[2]/div"))).getText();
				try
				{
				Assert.assertEquals(Exp_fr_btn, Act_fr_btn);
				test.log(LogStatus.PASS, "FR button is available" );
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "FR button is not available");	
				multiScreens.multiScreenShot(driver);
				}
			// Check AR button
				String Exp_ar_btn = "AR";
		 		String Act_ar_btn = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div/div[1]/div[2]/div[3]/div"))).getText();
				try
				{
				Assert.assertEquals(Exp_ar_btn, Act_ar_btn);
				test.log(LogStatus.PASS, "AR button is available" );
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "AR button is not available");	
				multiScreens.multiScreenShot(driver);
				}
		//Check placeholder text for 'Property name'
 		
		String Exp_Property_name = "Enter Property name, This name will be seen by guests when they search for a place to stay";
		String Act_Property_name =(driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Property name')]"))).getAttribute("placeholder");
		System.out.println(Act_Property_name);
 		try
		{
		Assert.assertEquals(Exp_Property_name, Act_Property_name);
		test.log(LogStatus.PASS, "Property name placeholder text is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Property name placeholder text is not available");	
		multiScreens.multiScreenShot(driver);
		}
		
 		//Star rating title verification
 		String Exp_rating_title = "Star rating";
 		String Act_rating_title = (driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[2]/div[2]/div/div[1]"))).getText();
		try
		{
		Assert.assertEquals(Exp_rating_title, Act_rating_title);
		test.log(LogStatus.PASS, "Star rating title is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Star rating title is not available");	
		multiScreens.multiScreenShot(driver);
		}
	// Rating field placeholder text validation
	
		String Exp_Rating = "N/A";
		String Act_Rating = (driver.findElement(By.xpath("//input[contains(@placeholder,'N/A')]"))).getAttribute("placeholder");
		try
		{
		Assert.assertEquals(Exp_Rating, Act_Rating);
		test.log(LogStatus.PASS, "Rating field placeholder text is available.");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Star rating placeholder is not available.");		
		multiScreens.multiScreenShot(driver);
		}

	//Address title verification
		String Exp_address_title = "Address";
 		String Act_address_title = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[3]/div/div/div[1]/div[1]"))).getText();
		try
		{
		Assert.assertEquals(Exp_address_title, Act_address_title);
		test.log(LogStatus.PASS, "Address title is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Address title is not available");	
		multiScreens.multiScreenShot(driver);
		}

	//Address placeholder field validation
		String Exp_Address = "Enter Address";
		String Act_Address = (driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Address')]"))).getAttribute("placeholder");
		try
		{
		Assert.assertEquals(Exp_Address, Act_Address);
		test.log(LogStatus.PASS, "Address placeholder text is available");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Address placeholder text is not available");
		multiScreens.multiScreenShot(driver);

		}
		//Country title verification
				String Exp_country_title = "Country";
		 		String Act_country_title = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[4]/div[1]/div/div[1]"))).getText();
				try
				{
				Assert.assertEquals(Exp_country_title, Act_country_title);
				test.log(LogStatus.PASS, "Country title is available" );
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Country title is not available");	
				multiScreens.multiScreenShot(driver);
				}
	// Country placeholder field verification 
		String Exp_country = "-- select country --";
		String Act_country = (driver.findElement(By.xpath("//input[contains(@placeholder,'-- select country --')]"))).getAttribute("placeholder");
		try
		{
		Assert.assertEquals(Exp_country, Act_country);
		test.log(LogStatus.PASS, "Country placeholder text is available");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Country placeholder text is not available");
		multiScreens.multiScreenShot(driver);
		}
		//City title verification
		String Exp_city_title = "City";
 		String Act_city_title = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[4]/div[2]/div/div[1]"))).getText();
		try
		{
		Assert.assertEquals(Exp_city_title, Act_city_title);
		test.log(LogStatus.PASS, "City title is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "City title is not available");	
		multiScreens.multiScreenShot(driver);
		}
	// City placeholder text verification
		String Exp_city = "-- select city --";
		String Act_city = (driver.findElement(By.xpath("//input[contains(@placeholder,'-- select city --')]"))).getAttribute("placeholder");
		try
		{
		Assert.assertEquals(Exp_city, Act_city);
		test.log(LogStatus.PASS, "City placeholder text is available");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "City placeholder text is not available");
		multiScreens.multiScreenShot(driver);

		}
		//Pincode title verification
				String Exp_pincode_title = "Pin code";
		 		String Act_pincode_title = (driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[4]/div[3]/div/div[1]/div"))).getText();
				try
				{
				Assert.assertEquals(Exp_pincode_title, Act_pincode_title);
				test.log(LogStatus.PASS, "Pin code title is available" );
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Pin code title is not available");	
				multiScreens.multiScreenShot(driver);
				}
		
	//Pincode placeholder field verification
		String Exp_pincode = "Enter pin code";
		String Act_pincode = (driver.findElement(By.xpath("//input[contains(@placeholder,'Enter pin code')]"))).getAttribute("placeholder");
		try
		{
		Assert.assertEquals(Exp_pincode, Act_pincode);
		test.log(LogStatus.PASS, "Pin code placeholder text is available");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Pin code placeholder text is not available");
		multiScreens.multiScreenShot(driver);

		}
	//Property type title verification
		String Exp_type_title = "Property Type";
 		String Act_type_title = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[5]/div[1]/div/div[1]"))).getText();
		try
		{
		Assert.assertEquals(Exp_type_title, Act_type_title);
		test.log(LogStatus.PASS, "Property type title is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Property type title is not available");	
		multiScreens.multiScreenShot(driver);
		}
		
		// Property type placeholder text verification
		String Exp_ptype_title = "-- select the type of property --";
 		String Act_ptype_title = (driver.findElement(By.xpath("//input[contains(@placeholder,'-- select the type of property --')]"))).getAttribute("placeholder");
		try
		{
		Assert.assertEquals(Exp_ptype_title, Act_ptype_title);
		test.log(LogStatus.PASS, "Property type placeholder text is available" );
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Property type placeholder text is not available");	
		multiScreens.multiScreenShot(driver);
		}
	
	//Total No. of rooms available in this property title verification
		String Exp_title_rooms = "Total number of rooms available in this property";
		String Act_title_rooms = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[5]/div[2]/div/div[1]"))).getText();
		try
		{
		Assert.assertEquals(Exp_title_rooms, Act_title_rooms);
		test.log(LogStatus.PASS, "Total number of rooms available in the property title is available");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Total number of rooms available in the property title is not available");
		multiScreens.multiScreenShot(driver);
		}
		//Total No. of rooms available in this property placeholder text verification
				String Exp_property_rooms = "Enter value";
				String Act_property_rooms = (driver.findElement(By.xpath("//input[contains(@placeholder,'Enter value')]"))).getAttribute("placeholder");
				try
				{
				Assert.assertEquals(Exp_property_rooms, Act_property_rooms);
				test.log(LogStatus.PASS, "Total number of rooms available in the property placeholder text is available");
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Total number of rooms available in the property placeholder text is not available");
				multiScreens.multiScreenShot(driver);
				}

		//Property policies title verification
				String Exp_ptitle_rooms = "Property policies";
				String Act_ptitle_rooms = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[6]/div/div/div[1]/div[1]"))).getText();
				try
				{
				Assert.assertEquals(Exp_ptitle_rooms, Act_ptitle_rooms);
				test.log(LogStatus.PASS, "Property policies title is available");
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Property policies title is not available");
				multiScreens.multiScreenShot(driver);
				}
//Property Policy placeholder text field validation
		String Exp_policies = "Add Policy";
		String Act_policies = (driver.findElement(By.xpath("//*[contains(@data-placeholder,'Add Policy')]"))).getAttribute("data-placeholder");
		try
		{
		Assert.assertEquals(Exp_policies, Act_policies);
		test.log(LogStatus.PASS, "Property Policies Placeholder text is available");
		multiScreens.multiScreenShot(driver);
		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Property policies placeholder text is not available ");
		multiScreens.multiScreenShot(driver);
		}
	//Property description title verification
		String Exp_pdescr_rooms = "Property Description";
		String Act_pdescr_rooms = (driver.findElement(By.xpath("/html/body/div/div/div/div[4]/div/div/div[7]/div/div/div[1]/div[1]"))).getText();
		try
		{
		Assert.assertEquals(Exp_pdescr_rooms, Act_pdescr_rooms);
		test.log(LogStatus.PASS, "Property description title is available");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Property description title is not available");
		multiScreens.multiScreenShot(driver);
		}	
		
	//Property description placeholder text field validation
		String Exp_description = "Add Description";
		String Act_description = (driver.findElement(By.xpath("//*[contains(@data-placeholder,'Add Description')]"))).getAttribute("data-placeholder");
		
		try
		{
		Assert.assertEquals(Exp_description, Act_description);
		test.log(LogStatus.PASS, "Property Description placeholder text is available .");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Property description placeholder text is not available");
		multiScreens.multiScreenShot(driver);

		}
	//Verify Cancellation and Refund policy title
		String Exp_cancel_title = "Cancellation and Refund policy";
		String Act_cancel_title = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[8]/div/div[1]"))).getText();
		try
		{
		Assert.assertEquals(Exp_cancel_title, Act_cancel_title);
		test.log(LogStatus.PASS, "Cancellation and refund policy title is available");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Cancellation and refund policy title is not available");
		multiScreens.multiScreenShot(driver);
		}	
	//Verify payment method text for cards
		String Exp_pay_text = "1. International card payment online, Wire Transfer & CIB Card payment";
		String Act_pay_text = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[8]/div/div[2]/div/div[1]"))).getText();
		try
		{
		Assert.assertEquals(Exp_pay_text, Act_pay_text);
		test.log(LogStatus.PASS, " Payment methods text is available. " + "Expected Result: " +Exp_pay_text);
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Payment methods text is not available. " + "Expected Result: "+Exp_pay_text);
		multiScreens.multiScreenShot(driver);
		}	
		
		//Verify payment method text for PAH
				String Exp_ph_text = "2. Pay at hotel";
				String Act_ph_text = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[8]/div/div[2]/div/div[3]"))).getText();
				try
				{
				Assert.assertEquals(Exp_ph_text, Act_ph_text);
				test.log(LogStatus.PASS, " Pay at hotel text is available. " + "Expected Result: " +Exp_ph_text);
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Pay at hotel text is not available. " + "Expected Result: "+Exp_ph_text);
				multiScreens.multiScreenShot(driver);
				}	
				
	//Ensure Ready cancellation button placeholder text verification
				String Exp_rcp_text = "Read Cancellation policies";
				String Act_rcp_text = (driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[8]/div/div[2]/div/div[5]/div/div"))).getText();
				try
				{
				Assert.assertEquals(Exp_rcp_text, Act_rcp_text);
				test.log(LogStatus.PASS, " Read cancellation policies button placeholder text is available " + "Expected Result: " +Exp_rcp_text);
				multiScreens.multiScreenShot(driver);

				}catch (AssertionError e)
				{
				test.log(LogStatus.FAIL, "Read cancellation policies button placeholder text is not available" + "Expected Result: "+Exp_rcp_text);
				multiScreens.multiScreenShot(driver);
				}
// Filling values at Basic info tab		
		driver.switchTo().parentFrame(); // Parent Frame
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
 		Thread.sleep(3000);
 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
 		Thread.sleep(3000);
 	
 	//Value updating for English language
 		driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Property name')]")).sendKeys("Fiona Palace");
 		driver.findElement(By.xpath("//input[contains(@placeholder,'N/A')]")).click();
 		driver.findElement(By.xpath("//*[(text()='4 Star')]")).click();
 		driver.findElement(By.xpath("//input[contains(@placeholder,'Address')]")).sendKeys("Puducherry");
		((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
 		WebElement country = driver.findElement(By.xpath("//input[contains(@placeholder,'-- select country --')]"));
 		country.click();
 		WebElement country1 = driver.findElement(By.xpath("//*[(text()='France')]")); //Country selection
 		country1.click();
 		driver.findElement(By.xpath("//input[contains(@placeholder,'-- select city --')]")).click(); //City selection 
 		WebElement city = driver.findElement(By.xpath("//*[(text()='Toulouse')]"));
 		city.click(); 
 		driver.findElement(By.xpath("//*[contains(@placeholder, 'Enter pin code')]")).sendKeys("650011"); //Pincode selection
 		driver.switchTo().parentFrame(); // Parent Frame
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,600)");
 		Thread.sleep(3000);
 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
 		Thread.sleep(3000);
 		driver.findElement(By.xpath("//input[contains(@placeholder,'-- select the type of property --')]")).click(); //Property selection
 		driver.findElement(By.xpath("//*[(text()='Hotel')]")).click();
		WebElement roomcount = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter value')]")); //Total no. of rooms selection
 		roomcount.sendKeys("10");
 		driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[6]/div/div/div[2]/div[2]/div[1]/div/div[2]/div[1]/p")).sendKeys("Test"); //Text area - Property Policy
		driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[7]/div/div/div[2]/div[2]/div[1]/div/div[2]/div[1]/p")).sendKeys("Test"); //Text area - Property description
 		driver.switchTo().parentFrame(); // Parent Frame
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
 		Thread.sleep(3000);
 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
 		Thread.sleep(3000);
 		
 		driver.switchTo().parentFrame(); // Parent Frame
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
 		Thread.sleep(3000);
 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
 		Thread.sleep(3000);
 		
try
{
 		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[3]/div")).click(); //Translate button selection and French convertion
 		Thread.sleep(3000);
		test.log(LogStatus.PASS, "System allowing to translate to French");
}
catch (AssertionError e)
{
test.log(LogStatus.FAIL, "System not allowing to translate to French");
}

try
{
 		driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[2]/div[3]/select")).click(); //Selection of language DD
 		driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[2]/div[3]/select")).sendKeys("AR"); //Arabic Convertion
 		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]/div/div/div[2]/div[3]/div")).click();  //Selecting Translate button
		test.log(LogStatus.PASS, "System allowing to translate to Arabic");
}
catch (AssertionError e)
{
test.log(LogStatus.FAIL, "System not allowing to translate to Arabic");	
}
 		multiScreens.multiScreenShot(driver);
 		driver.switchTo().parentFrame(); //Parent Frame
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame Navigation
 		Thread.sleep(3000);

try
{
 		driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[5]/div/div/div")).click(); //Save in Draft
 		Thread.sleep(3000);
		test.log(LogStatus.PASS, "System allowing to save in draft successfully");
}
catch (AssertionError e)
{
test.log(LogStatus.FAIL, "System not allowing to proceed save in draft");		
}

	try
	{
 		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[9]/div/div/div/div")).click(); //Next Button
 		multiScreens.multiScreenShot(driver);
		test.log(LogStatus.PASS, "System allowing to proceed with 'Next' button");
	}
	catch(AssertionError e)
	{
		test.log(LogStatus.FAIL, "System not allowing to proceed with 'Next' button");			
	}
	}
	
	//Contact details tab verification
	@Test(priority=6, invocationCount = 1)
	void contact_details() throws InterruptedException, IOException
	{
		test = report.startTest("Contact details Tab verification");
		driver.switchTo().parentFrame(); //Parent Frame
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		try
		{
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame Navigation
		Thread.sleep(3000);	
 		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[2]")).click(); //Navigation to Contact details Tab
		test.log(LogStatus.PASS, "Contact details tab loading properly");
		multiScreens.multiScreenShot(driver);

		}catch (AssertionError e)
		{
		test.log(LogStatus.FAIL, "Contact details tab is not loading properly");
		multiScreens.multiScreenShot(driver);

		}
	//Placeholder text for contact details tab
 		
		String cexp_title = "Contact details";
		String cact_title =driver.findElement(By.xpath("/html/body/div/div/div/div[4]/div/div/div/div[1]/div/div")).getText();

		try {
			Assert.assertEquals(cexp_title, cact_title);	
			test.log(LogStatus.PASS, "Contact details title available");
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Contact details title is not available");
			}
		multiScreens.multiScreenShot(driver);
//Verify the notification text
		String cexp_noti_title = "We will send notifications, booking related information and other important information on your primary and contact detail below.";
		String cact_noti_title =driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[1]/div/div/div[1]")).getText();

		try {
			Assert.assertEquals(cexp_noti_title, cact_noti_title);	
			test.log(LogStatus.PASS, "Notification text available. " + "Expected Result: " +cexp_noti_title);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Notification text is not available");
			}
		multiScreens.multiScreenShot(driver);
		// Verify primary mobile number title
		String cexp_pri_title = "Primary Mobile number:";
		String cact_pri_title =driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[1]/div/div/div[2]/div[1]/div[1]/div")).getText();

		try {
			Assert.assertEquals(cexp_pri_title, cact_pri_title);	
			test.log(LogStatus.PASS, "Primary mobile number title is available");
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Primary mobile number title is not available");
			}
		multiScreens.multiScreenShot(driver);
// Verify primary mobile number placeholder
		String cexp_pri_text = "Enter mobile number";
		String cact_pri_text =driver.findElement(By.xpath("//input[contains(@placeholder,'Enter mobile number')]")).getAttribute("placeholder");

		try {
			Assert.assertEquals(cexp_pri_text, cact_pri_text);	
			test.log(LogStatus.PASS, "Primary mobile number placeholder text is available");
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Primary mobile number placeholder text is not available");
			}
		multiScreens.multiScreenShot(driver);	
// Verify primary Email address title
		String cexp_prie_title = "Primary Email address:";
		String cact_prie_title =driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[1]/div/div/div[2]/div[2]/div[1]/div")).getText();

		try {
			Assert.assertEquals(cexp_prie_title, cact_prie_title);	
			test.log(LogStatus.PASS, "Primary email address title is available");
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Primary email address title is not available");
			}
		multiScreens.multiScreenShot(driver);
// Verify primary Email address placeholder
		String cexp_prie_text = "example@email.com";
		String cact_prie_text =driver.findElement(By.xpath("//input[contains(@placeholder,'example@email.com')]")).getAttribute("placeholder");

		try {
			Assert.assertEquals(cexp_prie_text, cact_prie_text);	
			test.log(LogStatus.PASS, "Primary email address placeholder text is available");
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL, "Primary email address placeholder text is not available");
			}
		multiScreens.multiScreenShot(driver);
		//Verify the notification text
				String cexp_noti1_title = "You may choose to add additional contacts. The contacts should not be the same as the Primary details.";
				String cact_noti1_title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[1]")).getText();

				try {
					Assert.assertEquals(cexp_noti1_title, cact_noti1_title);	
					test.log(LogStatus.PASS, "Notification for secondary contact details text available. " + "Expected Result: " +cexp_noti1_title);
					} catch (AssertionError e)
					{
						test.log(LogStatus.FAIL, "Notification for secondary contact details text is not available");
					}
				multiScreens.multiScreenShot(driver);					
	
	// Contact person Title verification
		String Act_sc_Title = "Contact Person 1:";
		String Exp_sc_Title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[2]")).getText();	
		try
			{
			Assert.assertEquals(Exp_sc_Title, Act_sc_Title);
			test.log(LogStatus.PASS,"Contact person title is available");
			multiScreens.multiScreenShot(driver);

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Contact person title is not available");
			multiScreens.multiScreenShot(driver);

			}
 	//Contact person salut title verification
		String Act_ssc_Title = "Select";
		String Exp_ssc_Title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[3]/div[1]/div/div[2]/div/div/div[1]")).getText();		
		try
			{
			Assert.assertEquals(Exp_ssc_Title, Act_ssc_Title);
			test.log(LogStatus.PASS,"Contact person salut placeholder text is available");
			multiScreens.multiScreenShot(driver);

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Contact person salut placeholder text is not available");
			multiScreens.multiScreenShot(driver);

			}
	
	// Contact person 1 - Firstname verification	
		String Exp_sc1_FN = "Enter first name";
		String Act_sc1_FN = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter first name')]")).getAttribute("placeholder");
				
		try
			{
			Assert.assertEquals(Exp_sc1_FN, Act_sc1_FN);
			test.log(LogStatus.PASS,"Secondary Person First name placeholder text is available");
			multiScreens.multiScreenShot(driver);

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Secondary Person First name placeholder text is not available");
			multiScreens.multiScreenShot(driver);

			}
 		
	// Contact person 1 - Lastname verification	
		String Act_sc1_LN = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter last name')]")).getAttribute("placeholder");
		String Exp_sc1_LN = "Enter last name";
				
		try
			{
			Assert.assertEquals(Exp_sc1_LN, Act_sc1_LN);
			test.log(LogStatus.PASS,"Secondary Person Last name placeholder text is available");
			multiScreens.multiScreenShot(driver);

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Secondary Person Last name placeholder text is available");
			multiScreens.multiScreenShot(driver);

			}
 				
	// Contact person 1 - Designation verification	
		String Act_sc1_Desig = driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[3]/div[4]/div/div[2]/div/div[1]/div[1]")).getText();
		String Exp_sc1_Desig = "--select designation--";
			
		try
			{
			Assert.assertEquals(Exp_sc1_Desig, Act_sc1_Desig);
			test.log(LogStatus.PASS,"Secondary Person Designation field placeholder text is present");
			multiScreens.multiScreenShot(driver);

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Either Secondary Person Designation placeholder text is not present");
			multiScreens.multiScreenShot(driver);

			}
		// Secondary phone number Title verification
				String Act_spn_Title = "Phone number:";
				String Exp_spn_Title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[1]/div[1]/div")).getText();		
				try
					{
					Assert.assertEquals(Exp_spn_Title, Act_spn_Title);
					test.log(LogStatus.PASS,"Phone number title is available");
					multiScreens.multiScreenShot(driver);

					} catch (AssertionError e)
					{
					test.log(LogStatus.FAIL, "Phone number title is not available");
					multiScreens.multiScreenShot(driver);

					}		

	// Contact person 1 - Secondary Phone number verification	
		String Act_sc1_Ph_no = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter phone number')]")).getAttribute("placeholder");
		String Exp_sc1_Ph_no = "Enter phone number";
				
		try
			{
			Assert.assertEquals(Exp_sc1_Ph_no, Act_sc1_Ph_no);
			test.log(LogStatus.PASS,"Secondary Person Phone number field placeholder text is available");
			multiScreens.multiScreenShot(driver);
		
			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Secondary Person Phone number field placeholder text is available");
			multiScreens.multiScreenShot(driver);

			}
	
		// Secondary mobile number Title verification
		String Act_smn_Title = "Mobile number:";
		String Exp_smn_Title = driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[2]/div[1]/div")).getText();		
		try
			{
			Assert.assertEquals(Exp_smn_Title, Act_smn_Title);
			test.log(LogStatus.PASS,"Mobile number title is available");
			multiScreens.multiScreenShot(driver);

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Mobile number title is not available");
			multiScreens.multiScreenShot(driver);

			}
	// Contact person 1 - Secondary mobile number verification	
		String Act_sc1_mob_no =driver.findElement(By.xpath("//input[contains(@placeholder,'Enter mobile number')]")).getAttribute("placeholder");
		String Exp_sc1_mob_no = "Enter mobile number";		
		try
			{
			Assert.assertEquals(Exp_sc1_mob_no, Act_sc1_mob_no);
			test.log(LogStatus.PASS,"Secondary Person mobile number placeholder text is present");
			multiScreens.multiScreenShot(driver);

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Either Secondary Person mobile number placeholder text is not present");
			multiScreens.multiScreenShot(driver);

			}
		// Secondary email address Title verification
				String Act_sea_Title = "Email address:";
				String Exp_sea_Title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[3]/div[1]/div")).getText();		
				try
					{
					Assert.assertEquals(Exp_sea_Title, Act_sea_Title);
					test.log(LogStatus.PASS,"Email Address title is available");
					multiScreens.multiScreenShot(driver);

					} catch (AssertionError e)
					{
					test.log(LogStatus.FAIL, "Email Address title is not available");
					multiScreens.multiScreenShot(driver);
					}	
	// Contact person 1 - Secondary email verification	
		String Act_sc1_mail = driver.findElement(By.xpath("//input[contains(@placeholder,'example@email.com')]")).getAttribute("placeholder");
		String Exp_sc1_mail = "example@email.com";
		try
			{
			Assert.assertEquals(Exp_sc1_mail, Act_sc1_mail);
			test.log(LogStatus.PASS,"Secondary Person email address placeholder text is present");
			multiScreens.multiScreenShot(driver);

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Secondary Person email address placeholder text is not present");
			multiScreens.multiScreenShot(driver);
			}
	//Assign role title verification
		String Act_sar_Title = "Assign Role :";
		String Exp_sar_Title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[5]/div[1]")).getText();		
		try
			{
			Assert.assertEquals(Exp_sar_Title, Act_sar_Title);
			test.log(LogStatus.PASS,"Assign role title is available");
			multiScreens.multiScreenShot(driver);

			} catch (AssertionError e)
			{
			test.log(LogStatus.FAIL, "Assign role title is not available");
			multiScreens.multiScreenShot(driver);
			}	
		//Add another contact title verification
				String Act_ac_Title = "Add another contact person";
				String Exp_ac_Title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[2]")).getText();		
				try
					{
					Assert.assertEquals(Exp_ac_Title, Act_ac_Title);
					test.log(LogStatus.PASS,"Add another contact person title link is available");
					multiScreens.multiScreenShot(driver);

					} catch (AssertionError e)
					{
					test.log(LogStatus.FAIL, "Add another contact person title link is not available");
					multiScreens.multiScreenShot(driver);
					}
		
	// Entering values for all fields
 		driver.switchTo().parentFrame(); //Parent frame navigation
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
 		Thread.sleep(3000);

 		WebElement Primary_mobile = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter mobile number')]"));
 		Primary_mobile.sendKeys("904723289"); //Primary mobile
		WebElement Primary_email = driver.findElement(By.xpath("//input[contains(@placeholder,'example@email.com')]"));
		Primary_email.sendKeys("example@email.com"); //Primary email

		driver.switchTo().parentFrame(); //Parent Frame navigation
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,400)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
	
		driver.findElement(By.xpath("//*[(text()='Select')]")).click();
		driver.findElement(By.xpath("//*[(text()='Mrs.')]")).click();
		driver.findElement(By.xpath("//input[contains(@placeholder,'Enter first name')]")).sendKeys("Josy");
		driver.findElement(By.xpath("//input[contains(@placeholder,'Enter last name')]")).sendKeys("Vimal");
		driver.findElement(By.xpath("//*[(text()='--select designation--')]")).click();;
		driver.findElement(By.xpath("//*[(text()='Manager')]")).click(); ////Designation
		WebElement Sec1_ph_no = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter phone number')]"));
		Sec1_ph_no.sendKeys("904723289"); // Sec phone no.
		WebElement Sec1_mob_no = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[2]/div[2]/input"));
		Sec1_mob_no.sendKeys("904723333"); //Sec mobile number
		WebElement Sec1_email = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[4]/div[3]/div[2]/input"));
		Sec1_email.sendKeys("example01@email.com"); //Secondary email
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div[2]/div/wrapper/div[1]/div[5]/div[2]/div[2]/input")).click(); //Assign role
		
		//Click on save in draft button
 		multiScreens.multiScreenShot(driver);
 		driver.switchTo().parentFrame(); //Parent frame navigation
 		Thread.sleep(3000);
 		((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
 		Thread.sleep(3000);
 		try
 		{
 		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[5]/div/div/div")).click(); // Save in Draft
 		Thread.sleep(3000);
 		multiScreens.multiScreenShot(driver);
		test.log(LogStatus.PASS, "Save in Draft button is available and allowed to select the same and proceed further");
 		}
 		catch (AssertionError e)
 		{
 			test.log(LogStatus.FAIL, "Either Save in Draft button is not available or unable to select and proceed further");
 		}

String Exp_cont_Prev_btn = "< Previous";
String Act_Cont_Prev_btn = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div/div/div[2]/div")).getText();
try
{
	Assert.assertEquals(Exp_cont_Prev_btn, Act_Cont_Prev_btn);
		test.log(LogStatus.PASS, "Previous button is available to navigate ' Basic info tab '.");
		multiScreens.multiScreenShot(driver);
}
catch (AssertionError e)
{
test.log(LogStatus.FAIL, "Previous button is available to navigate 'Basic info tab '.");
}
//next button
try
{
 		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div/div/div[3]")).click(); 	// Next button
		test.log(LogStatus.PASS, "Next button is available and allowed to select the same and proceed further");
}
catch (AssertionError e)
{
test.log(LogStatus.FAIL, "Either Next button is not available or not allowed to select the same and proceed further");
}
		}	

	@Test(priority = 7, invocationCount = 1)
	void photo_gallery() throws InterruptedException, AWTException, IOException
	{
	//Photo Gallery
		test = report.startTest("Validation of Photo Gallery");
		try {
		driver.switchTo().parentFrame(); //Parent Frame
	 		Thread.sleep(3000);
	 		((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	 		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame Navigation
	 		Thread.sleep(3000);
	 		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[3]")).click(); //Navigation to Photo Gallery Tab
	 		test.log(LogStatus.PASS, "Photo Gallery Tab loading properly");
	 		multiScreens.multiScreenShot(driver);
		} catch (AssertionError e)
		{
	 		test.log(LogStatus.FAIL, "Photo Gallery Tab is not loading properly");
	 		multiScreens.multiScreenShot(driver);
		}
	
	//Photo gallery - Title sentence 1
		
		String Act_title1 = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[1]/div/div/div")).getText();
		String Exp_title1 = "Travelers interact with photos more than any other part of your property listing, and the right ones can make a difference.";
		try
		{
		Assert.assertEquals(Exp_title1, Act_title1);
		test.log(LogStatus.PASS,"Title sentence 1: " +"\"" +Exp_title1+ "\"");
		multiScreens.multiScreenShot(driver);
		} catch (AssertionError e)
		{
			test.log(LogStatus.FAIL,"Title sentence 1 not available");	
			multiScreens.multiScreenShot(driver);
		}
	//Photo gallery - Title sentence 2
		
				String Act_title2 = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[1]")).getText();
				String Exp_title2 = "Upload hotel thumbnail photos(minimum of 350*250 px resolution, each photo not more than 2 MB size)";
				try
				{
				Assert.assertEquals(Exp_title2, Act_title2);
				test.log(LogStatus.PASS,"Title sentence 2: " +"\"" +Exp_title2+ "\"");
				multiScreens.multiScreenShot(driver);
				} catch (AssertionError e)
				{
					test.log(LogStatus.FAIL,"Title sentence 2 not available");	
					multiScreens.multiScreenShot(driver);
				}	
			/*	//Photo gallery - Title sentence 3
				
				String Act_title3 = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[3]/div/div/div[2]/div[1]")).getText();
				String Exp_title3 = "Upload hotel photos";
				try
				{
				Assert.assertEquals(Exp_title3, Act_title3);
				test.log(LogStatus.PASS,"Title sentence 3: " +"\"" +Exp_title3+ "\"");
				multiScreens.multiScreenShot(driver);
				} catch (AssertionError e)
				{
					test.log(LogStatus.FAIL,"Title sentence 3 not available");	
					multiScreens.multiScreenShot(driver);
				}	
			*/	
				//Placeholder text for image upload
				String Act_ptext = driver.findElement(By.xpath("/html/body/div/div/div/div[4]/div/div/div/div[3]/div/div/div[2]/div[2]/div/div/div")).getText();
				String Exp_ptext = "Drop files here or click to upload.";
				try
				{
				Assert.assertEquals(Exp_ptext, Act_ptext);
				test.log(LogStatus.PASS,"Drop files placeholder text is available");
				multiScreens.multiScreenShot(driver);
				} catch (AssertionError e)
				{
					test.log(LogStatus.FAIL,"Drop files placeholder text is not available");	
					multiScreens.multiScreenShot(driver);
				}	
				
	//Uploading all images.
		//Image 1
	 			WebElement Imgupl = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[2]/div[1]/div[2]/div[1]/div"));
	 			Imgupl.click();
	 			Thread.sleep(3000);
	 			Robot image1= new Robot();
	 			// copying File path to Clipboard
	 				StringSelection str1 = new StringSelection("C:\\Automation\\Images\\img1.jpg");
	 						
	 				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str1, null);	
	 			// press Contol+V for pasting
	 				image1.keyPress(KeyEvent.VK_CONTROL);
	 				image1.keyPress(KeyEvent.VK_V);
	 
	 			// release Contol+V for pasting
	 				image1.keyRelease(KeyEvent.VK_CONTROL);
	 				image1.keyRelease(KeyEvent.VK_V);
	     
	 			// for pressing and releasing Enter
	 				image1.keyPress(KeyEvent.VK_ENTER);
	 				image1.keyRelease(KeyEvent.VK_ENTER);
	 				Thread.sleep(3000);
	 				multiScreens.multiScreenShot(driver);
	 				driver.switchTo().parentFrame(); //Parent frame navigation
	 				Thread.sleep(3000);
	 				((JavascriptExecutor)driver).executeScript("scroll(0,600)");
	 				driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
	 				Thread.sleep(3000);
	 				driver.switchTo().activeElement().findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div/div/div[3]/div[2]")).click(); 		
	 				Thread.sleep(3000);
	 				driver.switchTo().parentFrame(); //Parent frame navigation
	 				Thread.sleep(3000);
	 				((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	 				driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
	 				multiScreens.multiScreenShot(driver);
		
	 //2nd image from 2 list
	 	WebElement Imgupl2 = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div"));
	 	Imgupl2.click();
	 	Thread.sleep(3000);
	 	Robot image2= new Robot();
	 	// copying File path to Clipboard
	 				StringSelection str2 = new StringSelection("C:\\Automation\\Images\\img2.jpg");
	 				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str2, null);	 	
	 			// press Contol+V for pasting
	 				image2.keyPress(KeyEvent.VK_CONTROL);
	 				image2.keyPress(KeyEvent.VK_V);
	 			// release Contol+V for pasting
	 				image2.keyRelease(KeyEvent.VK_CONTROL);
	 				image2.keyRelease(KeyEvent.VK_V);
	 			// for pressing and releasing Enter
	 				image2.keyPress(KeyEvent.VK_ENTER);
	 				image2.keyRelease(KeyEvent.VK_ENTER);
	 				Thread.sleep(3000);
	 				driver.switchTo().parentFrame(); //Parent frame navigation
	 				Thread.sleep(3000);
	 				((JavascriptExecutor)driver).executeScript("scroll(0,600)");
	 				driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
	 				Thread.sleep(3000);
	 				driver.switchTo().activeElement().findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div/div/div[3]/div[2]")).click();
	 				multiScreens.multiScreenShot(driver); 
	 				
	 	//1st image from 5 list
	 				Thread.sleep(3000);
	 				driver.switchTo().parentFrame(); //Parent frame navigation
	 				Thread.sleep(3000);
	 				((JavascriptExecutor)driver).executeScript("scroll(0,700)");
	 				driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation			
	 				WebElement Imgupl3 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
	 				Imgupl3.click();
	 				Thread.sleep(3000); 	
	 				Robot image3= new Robot();
	 			// copying File path to Clipboard
	 				StringSelection str3 = new StringSelection("C:\\Automation\\Images\\img3.jpg");
	 				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str3, null);
		  
	 			// press Contol+V for pasting
	 				image3.keyPress(KeyEvent.VK_CONTROL);
	 				image3.keyPress(KeyEvent.VK_V);
		 
	 			// release Contol+V for pasting
	 				image3.keyRelease(KeyEvent.VK_CONTROL);
	 				image3.keyRelease(KeyEvent.VK_V); 
	 			// for pressing and releasing Enter
	 				image3.keyPress(KeyEvent.VK_ENTER);
	 				image3.keyRelease(KeyEvent.VK_ENTER);
	 				multiScreens.multiScreenShot(driver); 
	 		 		
	 	
	 	//2nd image from 5 list
	 	Thread.sleep(3000);
	 				driver.switchTo().parentFrame(); //Parent frame navigation
	 				Thread.sleep(3000);
	 				((JavascriptExecutor)driver).executeScript("scroll(0,700)");
	 				driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
	 				WebElement Imgupl4 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
	 				Imgupl4.click();
	 				Thread.sleep(3000);
	 				Robot image4= new Robot();
	 			// copying File path to Clipboard
	 				StringSelection str4 = new StringSelection("C:\\Automation\\Images\\img4.jpg");
	 				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str4, null);
	
	 			// press Contol+V for pasting
	 				image4.keyPress(KeyEvent.VK_CONTROL);
	 				image4.keyPress(KeyEvent.VK_V);
	 			// release Contol+V for pasting
	 				image4.keyRelease(KeyEvent.VK_CONTROL);
	 				image4.keyRelease(KeyEvent.VK_V);		     
	 			// for pressing and releasing Enter
	 				image4.keyPress(KeyEvent.VK_ENTER);
	 				image4.keyRelease(KeyEvent.VK_ENTER);
	 				multiScreens.multiScreenShot(driver);
	 	
	 	//3rd image from 5 list
	 				Thread.sleep(3000);
	 				driver.switchTo().parentFrame(); //Parent frame navigation
	 				Thread.sleep(3000);
	 				((JavascriptExecutor)driver).executeScript("scroll(0,700)");
	 				driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation	
	 				WebElement Imgupl5 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
	 				Imgupl5.click();
	 				Thread.sleep(3000);
			 	
	 				Robot image5= new Robot();
	 			// copying File path to Clipboard
	 				StringSelection str5 = new StringSelection("C:\\Automation\\Images\\img5.jpg");
	 				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str5, null);	
	 			// press Contol+V for pasting
	 				image5.keyPress(KeyEvent.VK_CONTROL);
	 				image5.keyPress(KeyEvent.VK_V);
			    // release Contol+V for pasting
	 				image5.keyRelease(KeyEvent.VK_CONTROL);
	 				image5.keyRelease(KeyEvent.VK_V);
			    // for pressing and releasing Enter
	 				image5.keyPress(KeyEvent.VK_ENTER);	
	 				image5.keyRelease(KeyEvent.VK_ENTER); 
	 				multiScreens.multiScreenShot(driver);
	 	
	 	//4th image from 5 list
		
	 	Thread.sleep(3000);
				   driver.switchTo().parentFrame(); //Parent frame navigation
				   Thread.sleep(3000);
				   ((JavascriptExecutor)driver).executeScript("scroll(0,700)");
				   driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
					WebElement Imgupl6 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
				 	Imgupl6.click();
				 	Thread.sleep(3000);
				 	Robot image6= new Robot();
				 // copying File path to Clipboard
				    StringSelection str6 = new StringSelection("C:\\Automation\\Images\\img1.jpg");
				    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str6, null);
				  
				 // press Contol+V for pasting
				    image6.keyPress(KeyEvent.VK_CONTROL);
				    image6.keyPress(KeyEvent.VK_V);
				 
				 // release Contol+V for pasting
				    image6.keyRelease(KeyEvent.VK_CONTROL);
				    image6.keyRelease(KeyEvent.VK_V);
				     
				 // for pressing and releasing Enter
				    image6.keyPress(KeyEvent.VK_ENTER);
				    image6.keyRelease(KeyEvent.VK_ENTER);
				    multiScreens.multiScreenShot(driver);
		
		//5th image from 5 list
				    Thread.sleep(3000);
					   driver.switchTo().parentFrame(); //Parent frame navigation
					   Thread.sleep(3000);
					   ((JavascriptExecutor)driver).executeScript("scroll(0,700)");
					   driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
						WebElement Imgupl7 = driver.findElement(By.xpath("//*[(text()='Drop files here or click to upload.')]"));
					 	Imgupl7.click();
					 	Thread.sleep(3000);
					 	Robot image7= new Robot();
					 // copying File path to Clipboard
					    StringSelection str7 = new StringSelection("C:\\Automation\\Images\\img2.jpg");
					    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str7, null);
					  
					 // press Contol+V for pasting
					    image7.keyPress(KeyEvent.VK_CONTROL);
					    image7.keyPress(KeyEvent.VK_V);
					 
					 // release Contol+V for pasting
					    image7.keyRelease(KeyEvent.VK_CONTROL);
					    image7.keyRelease(KeyEvent.VK_V);
					     
					 // for pressing and releasing Enter
					    image7.keyPress(KeyEvent.VK_ENTER);
					    image7.keyRelease(KeyEvent.VK_ENTER);
					    multiScreens.multiScreenShot(driver);
	
	}

//Room type tab validation
	@Test (priority = 8, invocationCount = 1)
	void room_type() throws InterruptedException, IOException, AWTException
		{
		test = report.startTest("Room type tab validation");
			try
	 		{	
		 	driver.switchTo().parentFrame(); //Parent Frame
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");	
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame navigation
	
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[4]")).click(); //Navigation to Room Type Tab
			test.log(LogStatus.PASS, "Room type tab is loading properly");
	 		}
	 		catch (AssertionError e)
	 		{
			test.log(LogStatus.FAIL, "Room type tab is not loading properly");
	 		}
			
			// Enter Room type title verification
			String Act_erttitle = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[1]")).getText();
			String Exp_erttitle = "Enter room type";
			try
			{
			Assert.assertEquals(Exp_erttitle, Act_erttitle);
			test.log(LogStatus.PASS,"Enter Room type title is available");
			multiScreens.multiScreenShot(driver);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL,"Enter Room type title is not available");	
				multiScreens.multiScreenShot(driver);
			}	
			
	// Room type title verification
			String Act_rttitle = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[1]/div[1]/div/div[1]")).getText();
			String Exp_rttitle = "Room Type";
			try
			{
			Assert.assertEquals(Exp_rttitle, Act_rttitle);
			test.log(LogStatus.PASS,"Room type title is available");
			multiScreens.multiScreenShot(driver);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL,"Room type title is not available");	
				multiScreens.multiScreenShot(driver);
			}	
			
			
			//Room type placeholder text verification
			String Act_room_type = (driver.findElement(By.xpath("//input[contains(@placeholder,'--Select room type--')]"))).getAttribute("placeholder");
			String Exp_room_type = "--Select room type--";
			try
	 		{
				Assert.assertEquals(Exp_room_type, Act_room_type);
				test.log(LogStatus.PASS, "Room type Placeholder text is available ");
				multiScreens.multiScreenShot(driver);
	 		}
	 		catch (AssertionError e)
	 		{
			test.log(LogStatus.FAIL, "Room type placeholder text is not available");
			multiScreens.multiScreenShot(driver);
	 		}
		/*	
			//Room type1 placeholder text verification
		//Actions action = new Actions(driver);
		//String Act_room_type1 = (driver.findElement(By.xpath("//span[contains(@class,'Classic')]"))).getAttribute("class");
		//action.moveToElement(Act_room_type1);
			String Act_room_type1 = driver.findElement(By.xpath("//*[text()='Classic']")).getText();
			String Exp_room_type1 = "Classic";
			try
	 		{
				Assert.assertEquals(Exp_room_type1, Act_room_type1);
				test.log(LogStatus.PASS, "Room type1 Placeholder text is available "+Exp_room_type1);
				multiScreens.multiScreenShot(driver);
	 		}
	 		catch (AssertionError e)
	 		{
			test.log(LogStatus.FAIL, "Room type1 placeholder text is not available");
			multiScreens.multiScreenShot(driver);
	 		}
		*/	
	// Travellor category title verification
			String Act_tctitle = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[1]/div[2]/div/div[1]")).getText();
			String Exp_tctitle = "Traveller Category";
			try
			{
			Assert.assertEquals(Exp_tctitle, Act_tctitle);
			test.log(LogStatus.PASS,"Travellor category title is available");
			multiScreens.multiScreenShot(driver);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL,"Travellor category title is not available");	
				multiScreens.multiScreenShot(driver);
			}	
			
			//Travellor category placeholder text verification
			String Act_tr_cat = (driver.findElement(By.xpath("//input[contains(@placeholder,'--Select traveller category--')]"))).getAttribute("placeholder");
			String Exp_tr_cat = "--Select traveller category--";
			try
	 		{
				Assert.assertEquals(Exp_tr_cat, Act_tr_cat);
				test.log(LogStatus.PASS, "Travellor category placeholder text is available");
				multiScreens.multiScreenShot(driver);
	 		}
	 		catch (AssertionError e)
	 		{
			test.log(LogStatus.FAIL, "Either Travellor category placeholder text not available");
			multiScreens.multiScreenShot(driver);
	 		} 
//Total number of rooms available in this type at property  - Title verification
			String Act_tntitle = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[3]/div[1]/div/div[1]")).getText();
			String Exp_tntitle = "Total number of rooms available in this type at property";
			try
			{
			Assert.assertEquals(Exp_tntitle, Act_tntitle);
			test.log(LogStatus.PASS,"Total number of rooms available in this type at property title is available");
			multiScreens.multiScreenShot(driver);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL,"Total number of rooms available in this type at property title is not available");	
				multiScreens.multiScreenShot(driver);
			}	
			
	// No. of rooms available in property - Placeholder text
 			driver.switchTo().parentFrame(); //Parent frame navigation
 			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,350)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
			Thread.sleep(3000);
			
			String Act_rooms = (driver.findElement(By.xpath("//input[contains(@placeholder,'--Select room--')]"))).getAttribute("placeholder");
			String Exp_rooms = "--Select room--";
			try
			{
				Assert.assertEquals(Exp_rooms, Act_rooms);
				test.log(LogStatus.PASS, "Select room placeholder text is available - No. of rooms available in this porper");
				multiScreens.multiScreenShot(driver);
			}
	 		catch (AssertionError e)
	 		{
			test.log(LogStatus.FAIL, "Select room placeholder text is not available ");
			multiScreens.multiScreenShot(driver);
	 		}
	//Total number of rooms registering in this type with namlatic  - Title verification
			String Act_tmtitle = driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[3]/div[2]/div/div[1]")).getText();
			String Exp_tmtitle = "Total number of rooms registering in this type with Namlatic";
			try
			{
			Assert.assertEquals(Exp_tmtitle, Act_tmtitle);
			test.log(LogStatus.PASS,"Total number of rooms registering in this type at namlatic title is available");
			multiScreens.multiScreenShot(driver);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL,"Total number of rooms registering in this type at namlatic title is not available");	
				multiScreens.multiScreenShot(driver);
			}	
			
	// No. of rooms in namlatic - Placeholder text 
			String Act_nam_rooms = (driver.findElement(By.xpath("//input[contains(@placeholder,'--Select room--')]"))).getAttribute("placeholder");
			String Exp_nam_rooms = "--Select room--";
			try
			{
				Assert.assertEquals(Exp_nam_rooms, Act_nam_rooms);
				test.log(LogStatus.PASS, "Select room placeholder text is available - No. of rooms namlatic");
				multiScreens.multiScreenShot(driver);
			}
	 		catch (AssertionError e)
	 		{
			test.log(LogStatus.FAIL, "Select room placeholder text is not available - No. of rooms namlatic");
			multiScreens.multiScreenShot(driver);
	 		}
	//Amenities title
			String Act_tstitle = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[6]/div/div/div[1]")).getText();
			String Exp_tstitle = "Amenities";
			try
			{
			Assert.assertEquals(Exp_tstitle, Act_tstitle);
			test.log(LogStatus.PASS,"Amenities - Title is available");
			multiScreens.multiScreenShot(driver);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL,"Amenities - Title is not available");	
				multiScreens.multiScreenShot(driver);
			}	
	//Amenities placeholder text verification
			String Act_amenities = (driver.findElement(By.xpath("//input[contains(@placeholder,'--Select amenities--')]"))).getAttribute("placeholder");
			String Exp_amenities = "--Select amenities--";
			try
 			{
				Assert.assertEquals(Exp_amenities, Act_amenities);
				test.log(LogStatus.PASS, "Amenities field placeholder text is available");
				multiScreens.multiScreenShot(driver);
 			}
	 		catch (AssertionError e)
	 		{
			test.log(LogStatus.FAIL, "Amenities field placeholder text is not available.");
			multiScreens.multiScreenShot(driver);
	 		}
		
			//Popular filter title verification
			String Act_pftitle = driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[5]/div/div/div[1]")).getText();
			String Exp_pftitle = "Popular Filter";
			try
			{
			Assert.assertEquals(Exp_pftitle, Act_pftitle);
			test.log(LogStatus.PASS,"Popular filter - Title is available");
			multiScreens.multiScreenShot(driver);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL,"Popular filter - Title is not available");	
				multiScreens.multiScreenShot(driver);
			}	
	//Amenities placeholder text verification
			String Act_pf_ptext = (driver.findElement(By.xpath("//input[contains(@placeholder,'--Select popular filter--')]"))).getAttribute("placeholder");
			String Exp_pf_ptext = "--Select popular filter--";
			try
 			{
				Assert.assertEquals(Exp_pf_ptext, Act_pf_ptext);
				test.log(LogStatus.PASS, "Popular filter placeholder text is available");
				multiScreens.multiScreenShot(driver);
 			}
	 		catch (AssertionError e)
	 		{
			test.log(LogStatus.FAIL, "Popular filter placeholder text is not available.");
			multiScreens.multiScreenShot(driver);
	 		}
	/*//Photo upload title verification
			String Act_photo = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[7]/div/div/div[2]/div[1]")).getText();
			String Exp_photo = "Upload room photo(minimum 2 photos)";
			try
 			{
				Assert.assertEquals(Exp_photo, Act_photo);
				test.log(LogStatus.PASS, "Upload room photo title is available");
				multiScreens.multiScreenShot(driver);
 			}
	 		catch (AssertionError e)
	 		{
			test.log(LogStatus.FAIL, "Upload room photo title is not available");
			multiScreens.multiScreenShot(driver);
	 		}
	 */
	//Popular filter title verification
			String Act_artitle = driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/wrapper/div[1]")).getText();
			String Exp_artitle = "Add room";
			try
			{
			Assert.assertEquals(Exp_artitle, Act_artitle);
			test.log(LogStatus.PASS,"Add room link - Title is available");
			multiScreens.multiScreenShot(driver);
			} catch (AssertionError e)
			{
				test.log(LogStatus.FAIL,"Add room link - Title is not available");	
				multiScreens.multiScreenShot(driver);
			}	
			driver.switchTo().parentFrame(); //Parent frame navigation
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
			Thread.sleep(3000);
			
	//Entering all values
		driver.findElement(By.xpath("//*[contains(@placeholder,'--Select room type--')]")).click();
		driver.findElement(By.xpath("//*[text()='Classic']")).click();
	
		driver.switchTo().parentFrame(); //Parent frame navigation
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("scroll(0,350)");
		driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[contains(@placeholder,'--Select room--')]")).click();
		WebElement avl_count = driver.findElement(By.xpath("//*[text()='99']"));
		avl_count.click();
		
		driver.findElement(By.xpath("//*[contains(@placeholder,'--Select room--')]")).click();
		WebElement count = driver.findElement(By.xpath("//*[text()='90']"));
		count.click();
		
		//Sharing 1
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[1]/div/div[1]/div/input")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[1]/div/div[4]/div[2]/input[1]")).sendKeys("25.23");
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[1]/div/div[4]/div[3]/input[1]")).sendKeys("60.58");
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[1]/div/div[4]/div[4]/input[1]")).sendKeys("3410.12");
			Thread.sleep(2000);	 			
			multiScreens.multiScreenShot(driver);
		
	   //Sharing 2
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[2]/div/div[1]/div/input")).click();
		 	driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[2]/div/div[4]/div[2]/input[1]")).sendKeys("25.23");
		  	Thread.sleep(2000);
		  	driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[2]/div/div[4]/div[3]/input[1]")).sendKeys("60.58");
		  	Thread.sleep(2000);
		  	driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[2]/div/div[4]/div[4]/input[1]")).sendKeys("3410.12");
		  	Thread.sleep(2000);	 			
		  	
		  	driver.switchTo().parentFrame(); //Parent Frame
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
			Thread.sleep(3000);
			//((JavascriptExecutor)driver).executeScript("scroll(0,0)");	
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame navigation
		  	
			
		//Selection of amenities
		  	driver.findElement(By.xpath("//*[@placeholder='--Select amenities--']")).click();
		  	driver.findElement(By.xpath("//*[text()='Room Service']")).click();
			
		  	driver.switchTo().defaultContent();
		  	multiScreens.multiScreenShot(driver);
		 	driver.switchTo().parentFrame(); //Parent frame navigation
		 	Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,900)");
			Thread.sleep(3000);
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
			Thread.sleep(3000);
			multiScreens.multiScreenShot(driver);
			
		//Sharing 3
			driver.switchTo().parentFrame(); //Parent frame navigation
		 	Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,800)");
			Thread.sleep(3000);
			
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[3]/div/div[1]/div/input")).click();
		 	Thread.sleep(3000);
		  	driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[3]/div/div[4]/div[2]/input[1]")).sendKeys("30.89");
		  	Thread.sleep(2000);
		  	driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[3]/div/div[4]/div[3]/input[1]")).sendKeys("60.58");
		  	Thread.sleep(2000);
		  	driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/div/div[4]/div/wrapper/div[2]/div[3]/div/div[4]/div[4]/input[1]")).sendKeys("3410.12");
		  	Thread.sleep(2000);
		  	multiScreens.multiScreenShot(driver);
			
		 //Selection of Popular filter
		  	driver.switchTo().parentFrame(); //Parent frame navigation
		  	Thread.sleep(3000);
		  	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		  	Thread.sleep(3000);
		  	((JavascriptExecutor)driver).executeScript("scroll(0,900)");
		  	Thread.sleep(3000);
		  	driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
		  	Thread.sleep(3000);
		  	
		  	driver.findElement(By.xpath("//*[@placeholder='--Select popular filter--']")).click();
			driver.findElement(By.xpath("//*[text()='Couple friendly']")).click();
		  	
			multiScreens.multiScreenShot(driver);
			driver.switchTo().defaultContent();
		 	driver.switchTo().parentFrame(); //Parent frame navigation
		 	Thread.sleep(3000);
		 	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		 	Thread.sleep(3000);
		 	((JavascriptExecutor)driver).executeScript("scroll(0,50)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
			Thread.sleep(3000);
			
		//Travellor category.
			driver.findElement(By.xpath("//*[contains(@placeholder,'--Select traveller category--')]")).click();
			driver.findElement(By.xpath("//*[text()='Couple']")).click();
		
			multiScreens.multiScreenShot(driver);	 	
			driver.switchTo().parentFrame(); //Parent frame navigation
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");
	 		Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,1100)"); 
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
			Thread.sleep(3000);
		
		//Photo upload
		 	WebElement roomimgupl1 = driver.findElement(By.xpath("//*[text()='Click to upload']"));
		 	roomimgupl1.click();
		 	Thread.sleep(3000); 
		 	Robot roomrb1= new Robot();
		// copying File path to Clipboard
		 	StringSelection roomstr1 = new StringSelection("C:\\Automation\\Images\\img2.jpg");
		 	Toolkit.getDefaultToolkit().getSystemClipboard().setContents(roomstr1, null);
		// press Control+V for pasting
		 	roomrb1.keyPress(KeyEvent.VK_CONTROL);
		 	roomrb1.keyPress(KeyEvent.VK_V);
		// release Control+V for pasting
		 	roomrb1.keyRelease(KeyEvent.VK_CONTROL);
		 	roomrb1.keyRelease(KeyEvent.VK_V);
		// for pressing and releasing Enter
		 	roomrb1.keyPress(KeyEvent.VK_ENTER);
		 	roomrb1.keyRelease(KeyEvent.VK_ENTER);
		 	Thread.sleep(3000);
		 	
		// 2nd Image upload
		 	driver.switchTo().parentFrame(); //Parent frame navigation
		  	Thread.sleep(3000);
		  	((JavascriptExecutor)driver).executeScript("scroll(0,0)");
		  	Thread.sleep(3000);
		  	((JavascriptExecutor)driver).executeScript("scroll(0,1200)");
		  	Thread.sleep(3000);
		  	driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
		  	Thread.sleep(3000);
		  	
		 	WebElement roomimgupl2 = driver.findElement(By.xpath("//*[text()='Click to upload']"));
		 	roomimgupl2.click();
		 	Thread.sleep(3000);
		 	Robot roomrb2= new Robot();
		// copying File path to Clipboard
		 	StringSelection roomstr2 = new StringSelection("C:\\Automation\\Images\\img1.jpg");
		 	Toolkit.getDefaultToolkit().getSystemClipboard().setContents(roomstr2, null);
		//press Control+V for pasting
		 	roomrb2.keyPress(KeyEvent.VK_CONTROL);
		 	roomrb2.keyPress(KeyEvent.VK_V);
	    //release Control+V for pasting
		 	roomrb2.keyRelease(KeyEvent.VK_CONTROL);
		 	roomrb2.keyRelease(KeyEvent.VK_V);
	    //for pressing and releasing Enter
		 	roomrb2.keyPress(KeyEvent.VK_ENTER);
		 	roomrb2.keyRelease(KeyEvent.VK_ENTER);
		 	multiScreens.multiScreenShot(driver);
		 			
		 	driver.switchTo().parentFrame(); //Parent frame navigation
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
			driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/wrapper/wrapper/div[2]/div[2]")).click(); // Next button
		 	
		 	}
		
	//Social media validation
	@Test (priority = 9, invocationCount = 1)
		void social_media () throws InterruptedException, IOException, AWTException
	 		{
			test = report.startTest("Social media tab verification");
			driver.switchTo().parentFrame(); //Parent Frame
			Thread.sleep(3000);
			((JavascriptExecutor)driver).executeScript("scroll(0,0)");	
			
			try
				{
				driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame navigation
				Thread.sleep(3000);
				driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[5]")).click(); //Navigation to Social Type Tab
				multiScreens.multiScreenShot(driver);
				test.log(LogStatus.PASS, "Social media tab is loading properly");
				multiScreens.multiScreenShot(driver);
				}
				catch (AssertionError e)
				{
					test.log(LogStatus.FAIL, "Social media tab is not loading properly");
					multiScreens.multiScreenShot(driver);
				}
//Add social media title verification
			String Act_adtitle =driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div/div")).getText();
			String Exp_adtitle = "Add social media links (optional)";
		 	try
		 		{
		 		
		 		Assert.assertEquals(Exp_adtitle, Act_adtitle);
		 		
		 		test.log(LogStatus.PASS, "Add social media title is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Add social media is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}	 	
		 	//Facebook Title verification
			String Act_FBtitle =driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[1]/div[1]/div[2]")).getText();
			String Exp_FBtitle = "Facebook";
		 	try
		 		{
		 		
		 		Assert.assertEquals(Exp_FBtitle, Act_FBtitle);
		 		
		 		test.log(LogStatus.PASS, "Facebook title is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Facebook title is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}
		 	
		 	//Facebook placeholder text verification
		 	String Act_FB_ptext =(driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Facebook URL')]"))).getAttribute("placeholder");
			String Exp_FB_ptext = "Enter Facebook URL";
		 	try
		 		{
		 		
		 		Assert.assertEquals(Exp_FB_ptext, Act_FB_ptext);
		 		
		 		test.log(LogStatus.PASS, "Facebook Placeholder text is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Facebook placeholder text is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}
		 //Twitter title verification	
		 	String Act_TWtitle = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[2]/div[1]/div[2]")).getText();
			String Exp_TWtitle = "Twitter";
		 	try
		 		{
		 	
		 		Assert.assertEquals(Exp_TWtitle, Act_TWtitle);
		 		
		 		test.log(LogStatus.PASS, "Twitter title is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Twitter title is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}
		 	//Twitter placeholder text verification
		 	String Act_TW_ptext =(driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Twitter URL')]"))).getAttribute("placeholder");
			String Exp_TW_ptext = "Enter Twitter URL";
		 	try
		 		{
		 	
		 		Assert.assertEquals(Exp_TW_ptext, Act_TW_ptext);
		 		
		 		test.log(LogStatus.PASS, "Twitter Placeholder text is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Twitter Placeholder text is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}
		 	
		 	//Instagram Title verification
		 	String Act_IGtitle =driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[3]/div[1]/div[2]")).getText();
			String Exp_IGtitle = "Instagram";
		 	try
		 		{
		 		
		 		Assert.assertEquals(Exp_IGtitle, Act_IGtitle);
		 		
		 		test.log(LogStatus.PASS, "Instagram title is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Instagram title is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}
		 	//Instagram placeholder text verification
		 	String Act_IG_ptext =(driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Instagram URL')]"))).getAttribute("placeholder");
			String Exp_IG_ptext = "Enter Instagram URL";
		 	try
		 		{
		 	
		 		Assert.assertEquals(Exp_IG_ptext, Act_IG_ptext);
		 		
		 		test.log(LogStatus.PASS, "Instagram Placeholder text is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Instagram Placeholder text is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}
		 	//Linkedin title verification
			String Act_LNtitle =driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[4]/div[1]/div[2]")).getText();
			String Exp_LNtitle = "Linkedin";
		 	try
		 		{
		 	
		 		Assert.assertEquals(Exp_LNtitle, Act_LNtitle);
		 		
		 		test.log(LogStatus.PASS, "LinkedIn title is available.");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "LinkedIn title is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}
		 	//Linkedin placeholder text verification
		 	String Act_LN_ptext =(driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Linkedin URL')]"))).getAttribute("placeholder");
			String Exp_LN_ptext = "Enter Linkedin URL";
		 	try
		 		{
		 	
		 		Assert.assertEquals(Exp_LN_ptext, Act_LN_ptext);
		 		
		 		test.log(LogStatus.PASS, "Linkedin Placeholder text is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Linkedin Placeholder text is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}
		 	//Youtube title verification
		 	String Act_YTtitle =driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[5]/div[1]/div[2]")).getText();
			String Exp_YTtitle = "Youtube";
		 	try
		 		{
		 		
		 		Assert.assertEquals(Exp_YTtitle, Act_YTtitle);
		 		
		 		test.log(LogStatus.PASS, "Youtube title is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Youtube title is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}
		 	//Youtube placeholder text verification
		 	String Act_YT_ptext =(driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Youtube URL')]"))).getAttribute("placeholder");
			String Exp_YT_ptext = "Enter Youtube URL";
		 	try
		 		{
		 	
		 		Assert.assertEquals(Exp_YT_ptext, Act_YT_ptext);
		 		
		 		test.log(LogStatus.PASS, "Youtube Placeholder text is available");
		 		multiScreens.multiScreenShot(driver);
		 		}
		 		catch (AssertionError e)
		 		{
		 			test.log(LogStatus.FAIL, "Youtube Placeholder text is not available");
		 			multiScreens.multiScreenShot(driver);
		 		}	
		 	
		 	multiScreens.multiScreenShot(driver);
		  	driver.switchTo().parentFrame(); //Parent frame navigation
		 	Thread.sleep(3000);
		 	((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
		 	driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
		 	Thread.sleep(3000);
		 	
		 	driver.findElement(By.xpath("//*[@placeholder='Enter Facebook URL']")).clear();
		 	driver.findElement(By.xpath("//*[@placeholder='Enter Facebook URL']")).sendKeys("Example1@newmail.com");
		 	driver.findElement(By.xpath("//*[@placeholder='Enter Twitter URL']")).clear();
 		 	driver.findElement(By.xpath("//*[@placeholder='Enter Twitter URL']")).sendKeys("Example2@newmail.com");
 		 	driver.findElement(By.xpath("//*[@placeholder='Enter Instagram URL']")).clear();
 		 	driver.findElement(By.xpath("//*[@placeholder='Enter Instagram URL']")).sendKeys("Example3@newmail.com");
 		 	driver.findElement(By.xpath("//*[@placeholder='Enter Linkedin URL']")).clear();
 		 	driver.findElement(By.xpath("//*[@placeholder='Enter Linkedin URL']")).sendKeys("Example4@newmail.com");
 		 	driver.findElement(By.xpath("//*[@placeholder='Enter Youtube URL']")).clear();
 		 	driver.findElement(By.xpath("//*[@placeholder='Enter Youtube URL']")).sendKeys("Example5@newmail.com");
 		 	multiScreens.multiScreenShot(driver);
 		 	driver.switchTo().parentFrame(); //Parent frame navigation
		 	Thread.sleep(3000);
		 	((JavascriptExecutor)driver).executeScript("scroll(0,1600)");
		 	driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); //Frame navigation
		 	Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div/wrapper/div[6]/div[2]/div")).click(); 	// Next
	 		}

	//Finance & Legal
	@Test (priority = 10, invocationCount = 1)
	 	void finance() throws IOException, InterruptedException, AWTException
	 	{
	 	driver.switchTo().parentFrame(); //Parent Frame
	 	Thread.sleep(3000);
	 	((JavascriptExecutor)driver).executeScript("scroll(0,0)");			
	 	test = report.startTest("Finance & Legal tab verification");
	 	driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame navigation
	 	driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[6]")).click(); //Navigation to Finance & Legal Tab
	 
	 	//Registration incorporation document title verification
	 	String Act_ridtitle =driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[1]")).getText();
		String Exp_ridtitle = "Registration incorporation document";
	 	try
	 		{
	 		
	 		Assert.assertEquals(Exp_ridtitle, Act_ridtitle);
	 		
	 		test.log(LogStatus.PASS, "Registration incorporation document title is available");
	 		multiScreens.multiScreenShot(driver);
	 		}
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Registration incorporation document title is not available");
	 			multiScreens.multiScreenShot(driver);
	 		}
	 	
	 	//Document title verification
	 		String Act_doc_title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[1]/div[1]/div")).getText();
	 		String Exp_doc_title = "Title of the document you are attaching:";
	 		try
	 		{
	 			Assert.assertEquals(Exp_doc_title, Act_doc_title);
	 			test.log(LogStatus.PASS, "Document title is available");
	 			multiScreens.multiScreenShot(driver);
	 		} 
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Document title is not available");
	 			multiScreens.multiScreenShot(driver);
	 		}
	 	//Document title placeholder text verification
	 		String Act_doc_ptext = (driver.findElement(By.xpath("//input[contains(@placeholder,'--Select--')]"))).getAttribute("placeholder");
	 		String Exp_doc_ptext = "--Select--";
	 		try
	 		{
	 			Assert.assertEquals(Exp_doc_ptext, Act_doc_ptext);
	 			test.log(LogStatus.PASS, "Title of the Document placeholder text is available");
	 			multiScreens.multiScreenShot(driver);
	 		} 
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Title of the Document placeholder text is not available");
	 			multiScreens.multiScreenShot(driver);
	 		}
// Document created on title verification
	 		String Act_date1_title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]")).getText();
	 		String Exp_date1_title = "Document created on";
	 		try
	 		{
	 			Assert.assertEquals(Exp_date1_title, Act_date1_title);
	 			test.log(LogStatus.PASS, "Document created date on title is available");
	 			multiScreens.multiScreenShot(driver);
	 		} 
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Document created date on title is not available");
	 			multiScreens.multiScreenShot(driver);
	 		}
	 	// Document created on placeholder text verification
	 		String Act_date1_ptext = (driver.findElement(By.xpath("//input[contains(@placeholder,'--Select--')]"))).getAttribute("placeholder");
	 		String Exp_date1_ptext = "--Select--";
	 		try
	 		{
	 			Assert.assertEquals(Exp_date1_ptext, Act_date1_ptext);
	 			test.log(LogStatus.PASS, "Document created date on placeholder text is available");
	 			multiScreens.multiScreenShot(driver);
	 		} 
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Document created date on placeholder text is not available");
	 			multiScreens.multiScreenShot(driver);
	 		}
//Document valid till date title verification	 		
	 		String Act_date2_title = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]")).getText();
	 		String Exp_date2_title = "Document valid till";
	 		try
	 		{
	 			Assert.assertEquals(Exp_date2_title, Act_date2_title);
	 			test.log(LogStatus.PASS, "Document valid date is available");
	 			multiScreens.multiScreenShot(driver);
	 		} 
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Document valid till date is not available");
	 			multiScreens.multiScreenShot(driver);
	 		}
	 	// Document valid till date placeholder text verification
	 		String Act_date2_ptext = (driver.findElement(By.xpath("//input[contains(@placeholder,'--Select--')]"))).getAttribute("placeholder");
	 		String Exp_date2_ptext = "--Select--";
	 		try
	 		{
	 			Assert.assertEquals(Exp_date2_ptext, Act_date2_ptext);
	 			test.log(LogStatus.PASS, "Document valid till date placeholder text is available");
	 			multiScreens.multiScreenShot(driver);
	 		} 
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Document vaild till date placeholder text is not available");
	 			multiScreens.multiScreenShot(driver);
	 		}	 
	//Upload document title verification
	 		String Act_doc_upload = driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[2]/div/div[2]/div[1]/span")).getText();
	 		String Exp_doc_upload = "Upload document";
	 		try
	 		{
	 			Assert.assertEquals(Exp_doc_upload, Act_doc_upload);
	 			test.log(LogStatus.PASS, "Document upload title is available");
	 			multiScreens.multiScreenShot(driver);
	 		} 
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Document upload title is available");
	 			multiScreens.multiScreenShot(driver);
	 		}
	 		
	 	// Upload document placeholder text verification
	 		String Act_udoc_ptext = (driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[2]/div/div[2]/div[2]/div/div/div"))).getText();
	 		String Exp_udoc_ptext = "Drop files here or click to upload.";
	 		try
	 		{
	 			Assert.assertEquals(Exp_udoc_ptext, Act_udoc_ptext);
	 			test.log(LogStatus.PASS, "Upload document placeholder text is available");
	 			multiScreens.multiScreenShot(driver);
	 		} 
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Upload document placeholder text is not available");
	 			multiScreens.multiScreenShot(driver);
	 		}	
//Note message text verification
	 		String Act_note_ptext = (driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[2]/div/div[2]/div[3]"))).getText();
	 		String Exp_note_ptext = "Note: Document file size should be less than 2MB, Supported File format : JPG, PNG, PDF";
	 		try
	 		{
	 			Assert.assertEquals(Exp_note_ptext, Act_note_ptext);
	 			test.log(LogStatus.PASS, "Note text is available " +Exp_note_ptext);
	 			multiScreens.multiScreenShot(driver);
	 		} 
	 		catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Note text is not available");
	 			multiScreens.multiScreenShot(driver);
	 		}	
	 		
	 	//Entering values for all fields
	 		driver.findElement(By.xpath(" /html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div[1]/div[1]")).click();
 		 	driver.findElement(By.xpath("//*[text()='Companies register']")).click();
 		 	
 		//File Upload
 		 	WebElement imgupl = driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[2]/div/div[2]/div[2]/div/div"));
 		 	imgupl.click();
 		 	Thread.sleep(6000);	
 		 	Robot rb= new Robot();
 		// copying File path to Clipboard
 		 	StringSelection str = new StringSelection("C:\\Automation\\Images\\img1.jpg");
 		 	Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
 		// press Contol+V for pasting
 		 	rb.keyPress(KeyEvent.VK_CONTROL);
 		 	rb.keyPress(KeyEvent.VK_V); 
 		// release Contol+V for pasting
 		 	rb.keyRelease(KeyEvent.VK_CONTROL);
 		 	rb.keyRelease(KeyEvent.VK_V); 
 		// for pressing and releasing Enter
 		 	rb.keyPress(KeyEvent.VK_ENTER);
 		 	rb.keyRelease(KeyEvent.VK_ENTER);
 		 	multiScreens.multiScreenShot(driver);
		
 		 	driver.findElement(By.xpath("//*[@placeholder='--Select--']")).click();
 		 	driver.findElement(By.xpath("//*[text()='2']")).click();
 		 	driver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div/input")).click();
 		 	Thread.sleep(3000);
 		 	driver.findElement(By.xpath("//*[text()='10']")).click();
 		 	multiScreens.multiScreenShot(driver);
 		 	driver.switchTo().parentFrame(); //Parent Frame
 		 	Thread.sleep(3000);
 		 	((JavascriptExecutor)driver).executeScript("scroll(0,900)");		
 		 	driver.switchTo().frame(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/iframe"))); // Frame navigation
 		 	driver.findElement(By.xpath("//*[text()='FINISH, REGISTER THIS PROPERTY']")).click();
 		 	Thread.sleep(3000);
	 	}
	//Thank you page
	
			@Test (priority = 12, invocationCount = 1)
			public void thankyou_page_text_Verification() throws IOException, InterruptedException
			{
				test = report.startTest("Thank you page text verification");
			try
			{
			//Line 1
				String Act_thtext1 = (driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[1]/div"))).getText();
				String Exp_thtext1 = "Thank you";
				AssertJUnit.assertEquals(Exp_thtext1, Act_thtext1);
				System.out.println(Act_thtext1);
				test.log(LogStatus.PASS, " Line 1 verified " +Exp_thtext1);
			}
			catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Line 1 text is not found");
	 			multiScreens.multiScreenShot(driver);
	 		}	
				
			//Line 1
			try
			{
				String Exp_thtext2 ="for registering your property with Namlatic";
				String Act_thtext2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[2]")).getText();
				AssertJUnit.assertEquals(Exp_thtext2, Act_thtext2);
				test.log(LogStatus.PASS, " Line 2 verified " +Exp_thtext2);
			}
			catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Line 2 text is not found");
	 			multiScreens.multiScreenShot(driver);
	 		}	
			//Line 2
			try
			{
				String Exp_thtext3 ="Soon our team will review the property details and revert back with the enrollment feedback.";
				String Act_thtext3= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[3]")).getText();
				AssertJUnit.assertEquals(Exp_thtext3, Act_thtext3);
				test.log(LogStatus.PASS, " Line 3 verified " +Exp_thtext3);
			}
			catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Line 3 is not found");
	 			multiScreens.multiScreenShot(driver);
	 		}	
			//Line 3
			try
			{
				String Exp_thtext4 ="For further queries contact Namlatic Help Center";
				String Act_thtext4= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[4]")).getText();
				AssertJUnit.assertEquals(Exp_thtext4, Act_thtext4);
				test.log(LogStatus.PASS, " Line 4 verified " +Exp_thtext4);
			}
			catch (AssertionError e)
	 		{
	 			test.log(LogStatus.FAIL, "Line 4 text is not found");
	 			multiScreens.multiScreenShot(driver);
	 		}	
			//Okay button
			try
			{
				String Exp_thbtn ="Okay";
				String Act_thbtn= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div[5]/div/div")).getText();
				AssertJUnit.assertEquals(Exp_thbtn, Act_thbtn);
				test.log(LogStatus.PASS, "Thank you Page text verified, Pass");
				System.out.println("Okay text is found, Pass");
			}
				catch (AssertionError e)
				  {
					System.out.println("Thank you page text mismatches with expected");
					test.log(LogStatus.FAIL, "Okay text not found");
				  }
			}

	 	@AfterMethod
	 	public static void endMethod()
	 	{
	 		report.endTest(test);	
	 	}

	 	@AfterClass
	 	public static void endTest()
	 	{
	 		System.out.println("End");
	 	report.flush();
	 	report.close();
	 	//driver.quit();
	 	}

}